pn.rootDir      = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
addpath(genpath('/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/T_tools/SampEn/'))
addpath(genpath('/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/T_tools/legendmarkeradjust/'))

%% plot real timeseries

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/B_MSE_Segmented_Dim_Input/1117_EC_MSE_IN.mat')

cfg = [];
cfg.hpfilter = 'yes';
cfg.hpfreq = [20];
cfg.hpfiltord = 6;
cfg.hpfilttype = 'but';

data_HPF = ft_preprocessing(cfg, data);

indTrial = 35;
indChan = 15;

R1 = 0.5.*std(data.trial{indTrial}(indChan,:));
R2 = 0.5.*std(data_HPF.trial{indTrial}(indChan,:));

% get sample entropy alongside 2 and 3 point patterns

points = [1:3];
timeSamples = 500;

% data.trial{indTrial} = zscore(data.trial{indTrial}(:,1:timeSamples),[],2);
% data_HPF.trial{indTrial} = zscore(data_HPF.trial{indTrial}(:,1:timeSamples),[],2);

[Samp1, D_out1] = SampEn_withD_out(2, R1, data.trial{indTrial}(indChan,1:timeSamples));
[Samp2, D_out2] = SampEn_withD_out(2, R2, data_HPF.trial{indTrial}(indChan,1:timeSamples));

h = figure('units','normalized','position',[.1 .1 .4 .2]);

%subplot(3,1,1); 
    cla;
    hold on;
    tmpSignal = data_HPF.trial{indTrial}(indChan,1:1:timeSamples);
    time = 1/500:1/500:timeSamples/500;
    patches.ampVec = [tmpSignal(points(1))-(R2); tmpSignal(points(1))+(R2);...
        tmpSignal(points(2))-(R2); tmpSignal(points(2))+(R2);...
        tmpSignal(points(3))-(R2); tmpSignal(points(3))+(R2)];
    patches.timeVec = [time(1); time(timeSamples); time(2); time(timeSamples); time(3); time(timeSamples)];
    %patches.colorVec =[.9 .9 .9; 1 1 1; .85 .85 .85; 1 1 1; .8 .8 .8];
    patches.colorVec =[.9 .9 .9; 0 0 0; .8 .8 .8; 1 1 1; .7 .8 .7]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = .7;
    end
    % indicate edges using broken lines
    for indP = 1:2:size(patches.ampVec,1)-1
        ll1 = line([patches.timeVec(indP,1) patches.timeVec(indP+1,1)], [patches.ampVec(indP,1) patches.ampVec(indP,1)]);
        set(ll1, 'Color',patches.colorVec(indP,:), 'LineWidth', 2, 'LineStyle', ':')
        ll2 = line([patches.timeVec(indP,1) patches.timeVec(indP+1,1)], [patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)]);
        set(ll2, 'Color',patches.colorVec(indP,:), 'LineWidth', 2, 'LineStyle', ':')
    end
    plot(time(1:1:timeSamples), tmpSignal, 'k', 'LineWidth', 1, 'Marker', 'o','MarkerFaceColor',[1 1 1])
    % add first template point + add bars to indicate similarity bounds
    line([time(points(1)), time(points(1))], [tmpSignal(points(1))-(R2); tmpSignal(points(1))+(R2)], 'LineWidth', 1, 'Color', 'k');
    line([time(points(1)), time(points(1+1))], [tmpSignal(points(1))-(R2); tmpSignal(points(1))-(R2)], 'LineStyle', ':', 'LineWidth', 1, 'Color', 'k');
    line([time(points(1)), time(points(1+1))], [tmpSignal(points(1))+(R2); tmpSignal(points(1))+(R2)], 'LineStyle', ':', 'LineWidth', 1, 'Color', 'k');
    scatter(time(points(1)), tmpSignal(1,points(1)), 150, 'filled','diamond','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0])
    % add second template point
    line([time(points(2)), time(2)], [tmpSignal(2)-(R2); tmpSignal(2)+(R2)], 'LineWidth', 1, 'Color', 'k');
    line([time(points(2)), time(2+1)], [tmpSignal(2)-(R2); tmpSignal(2)-(R2)], 'LineStyle', ':', 'LineWidth', 1, 'Color', 'k');
    line([time(points(2)), time(2+1)], [tmpSignal(2)+(R2); tmpSignal(2)+(R2)], 'LineStyle', ':', 'LineWidth', 1, 'Color', 'k');
    leg1 = scatter(time(points(2)), tmpSignal(1,points(2)), 150, 'filled','diamond','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0]);
    %title({'20 Hz highpass-filtered signal:',['low similarity criterion, high sample entropy (', num2str(round(Samp2,2)),')']})
    xlabel('Time (s)'); ylabel('Amplitude (a.u.)');
    % add remaining two-and three point patterns
    for indTwo = 1:size(D_out2{2,1},2)
        scatter(time(D_out2{2,1}(indTwo)+1), tmpSignal(D_out2{2,1}(indTwo)+1), 120, 'filled','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0])
        leg3 = scatter(time(D_out2{2,1}(indTwo)+2), tmpSignal(D_out2{2,1}(indTwo)+2), 120, 'filled','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0]);
    end
    for indTwo = 1:size(D_out2{2,1},2)
        if max(ismember(D_out2{3,1},D_out2{2,1}(indTwo)))==1
            leg4 = scatter(time(D_out2{2,1}(indTwo)+3), tmpSignal(D_out2{2,1}(indTwo)+3), 120, 'filled','pentagram','MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
        else
            leg5 = scatter(time(D_out2{2,1}(indTwo)+3), tmpSignal(D_out2{2,1}(indTwo)+3), 120, 'filled','pentagram','MarkerFaceColor',[1,0,0], 'MarkerEdgeColor', [0 0 0]);
        end
    end
    % add third template point
    line([time(points(3)), time(3)], [tmpSignal(3)-(R2); tmpSignal(3)+(R2)], 'LineWidth', 1, 'Color', [0 0 0]);
    line([time(points(3)), time(3+1)], [tmpSignal(3)-(R2); tmpSignal(3)-(R2)], 'LineStyle', ':', 'LineWidth', 1, 'Color', [0 0 0]);
    line([time(points(3)), time(3+1)], [tmpSignal(3)+(R2); tmpSignal(3)+(R2)], 'LineStyle', ':', 'LineWidth', 1, 'Color', [0 0 0]);
    leg2 = scatter(time(points(3)), tmpSignal(points(3)), 150, 'filled','diamond', 'MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
    %legend('off')
    ylim([-14 9]*10^-4)
    xlim([0 .1])
    [legendHandle, legendIcons] = legend([leg1, leg2, leg3, leg4, leg5], {'Template m=2'; 'Template m=3'; 'Match m=2'; 'Match m=3'; 'Non-match m=3'}, ...
            'location', 'South', 'orientation', 'horizontal', 'FontSize', 12); legend('boxoff')%     legend('off')
    set(legendHandle, 'FontSize', 12)
    legendIcons = findobj(legendIcons, 'type', 'patch'); %// objects of legend of type line
    set(legendIcons, 'Markersize', 14); %// set marker size as desired
set(findall(gcf,'-property','FontSize'),'FontSize',16)
pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'D_RparamExample_Fig1';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot the original signal, both for the exemplary short period and for a longer example

h = figure('units','normalized','position',[.1 .1 .3 .3]);
cla; hold on;
    tmpSignal = data.trial{indTrial}(indChan,1:1:timeSamples);
    time = 1/500:1/500:timeSamples/500;
    patches.ampVec = [tmpSignal(points(1))-(R1); tmpSignal(points(1))+(R1);...
        tmpSignal(points(2))-(R1); tmpSignal(points(2))+(R1);...
        tmpSignal(points(3))-(R1); tmpSignal(points(3))+(R1)];
    patches.timeVec = [time(1); time(timeSamples); time(2); time(timeSamples); time(3); time(timeSamples)];
    %patches.colorVec =[.9 .9 .9; 1 1 1; .85 .85 .85; 1 1 1; .8 .8 .8];
    patches.colorVec =[.9 .9 .9; 0 0 0; .8 .8 .8; 1 1 1; .7 .8 .7]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = .7;
    end
    % indicate edges using broken lines
    for indP = 1:2:size(patches.ampVec,1)-1
        ll1 = line([patches.timeVec(indP,1) patches.timeVec(indP+1,1)], [patches.ampVec(indP,1) patches.ampVec(indP,1)]);
        set(ll1, 'Color',patches.colorVec(indP,:), 'LineWidth', 2, 'LineStyle', ':')
        ll2 = line([patches.timeVec(indP,1) patches.timeVec(indP+1,1)], [patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)]);
        set(ll2, 'Color',patches.colorVec(indP,:), 'LineWidth', 2, 'LineStyle', ':')
    end
    plot(time(1:1:timeSamples), tmpSignal, 'k', 'LineWidth', 1, 'Marker', 'o','MarkerFaceColor',[1 1 1])
    % add first template point + add bars to indicate similarity bounds
    line([time(points(1)), time(points(1))], [tmpSignal(points(1))-(R1); tmpSignal(points(1))+(R1)], 'LineWidth', 1, 'Color', 'k');
    line([time(points(1)), time(points(1+1))], [tmpSignal(points(1))-(R1); tmpSignal(points(1))-(R1)], 'LineStyle', '-', 'LineWidth', 1, 'Color', 'k');
    line([time(points(1)), time(points(1+1))], [tmpSignal(points(1))+(R1); tmpSignal(points(1))+(R1)], 'LineStyle', '-', 'LineWidth', 1, 'Color', 'k');
    scatter(time(points(1)), tmpSignal(1,points(1)), 150, 'filled','diamond','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0])
    % add second template point
    line([time(points(2)), time(2)], [tmpSignal(2)-(R1); tmpSignal(2)+(R1)], 'LineWidth', 1, 'Color', 'k');
    line([time(points(2)), time(2+1)], [tmpSignal(2)-(R1); tmpSignal(2)-(R1)], 'LineStyle', '-', 'LineWidth', 1, 'Color', 'k');
    line([time(points(2)), time(2+1)], [tmpSignal(2)+(R1); tmpSignal(2)+(R1)], 'LineStyle', '-', 'LineWidth', 1, 'Color', 'k');
    leg1 = scatter(time(points(2)), tmpSignal(1,points(2)), 150, 'filled','diamond','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0]);
        %title({'Broadband time domain signal:',['high similarity criterion, low sample entropy (', num2str(round(Samp1,2)),')']})
    xlabel('Time (s)'); ylabel('Amplitude (a.u.)');
    % add remaining two-and three point patterns
    for indTwo = 1:size(D_out1{2,1},2)
        scatter(time(D_out1{2,1}(indTwo)+1), tmpSignal(D_out1{2,1}(indTwo)+1), 120, 'filled','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0])
        leg3 = scatter(time(D_out1{2,1}(indTwo)+2), tmpSignal(D_out1{2,1}(indTwo)+2), 120, 'filled','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0]);
    end
    for indTwo = 1:size(D_out1{2,1},2)
        if max(ismember(D_out1{3,1},D_out1{2,1}(indTwo)))==1
            leg4 = scatter(time(D_out1{2,1}(indTwo)+3), tmpSignal(D_out1{2,1}(indTwo)+3), 120, 'filled','pentagram','MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
        else
            leg5 = scatter(time(D_out1{2,1}(indTwo)+3), tmpSignal(D_out1{2,1}(indTwo)+3), 120, 'filled','pentagram','MarkerFaceColor',[1,0,0], 'MarkerEdgeColor', [0 0 0]);
        end
    end
    % add third template point
    line([time(points(3)), time(3)], [tmpSignal(3)-(R1); tmpSignal(3)+(R1)], 'LineWidth', 1, 'Color', [0 0 0]);
    line([time(points(3)), time(3+1)], [tmpSignal(3)-(R1); tmpSignal(3)-(R1)], 'LineStyle', '-', 'LineWidth', 1, 'Color', [0 0 0]);
    line([time(points(3)), time(3+1)], [tmpSignal(3)+(R1); tmpSignal(3)+(R1)], 'LineStyle', '-', 'LineWidth', 1, 'Color', [0 0 0]);
    leg2 = scatter(time(points(3)), tmpSignal(points(3)), 150, 'filled','diamond', 'MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
    %legend('off')
    ylim([-7 -.1]*10^-3)
    xlim([0 .1])

% add inset of longer fluctuations
handaxes2 = axes('Position', [0.18 0.7 0.7 .15]); cla;
    hold on;
    plot(time(1:1:timeSamples), tmpSignal, 'k', 'LineWidth', 1, 'Marker', '.','MarkerFaceColor',[1 1 1])
    for indTwo = 1:size(D_out1{2,1},2)
        if max(ismember(D_out1{3,1},D_out1{2,1}(indTwo)))==1
            leg4 = scatter(time(D_out1{2,1}(indTwo)+3), tmpSignal(D_out1{2,1}(indTwo)+3), 25, 'filled','MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
        else
            leg5 = scatter(time(D_out1{2,1}(indTwo)+3), tmpSignal(D_out1{2,1}(indTwo)+3), 25, 'filled','MarkerFaceColor',[1,0,0], 'MarkerEdgeColor', [0 0 0]);
        end
    end
    ylim([-6 -1]*10^-3)
    xlim([0 .8])
    % plot box around the signal shown in the main plot
    curYlim = get(gca, 'Ylim'); 
    line([0 0], [curYlim(1),curYlim(2)], 'Color', 'r', 'LineWidth', 2, 'LineStyle', ':')
    line([0 .1], [curYlim(1),curYlim(1)], 'Color', 'r', 'LineWidth', 2, 'LineStyle', ':')
    line([0 .1], [curYlim(2),curYlim(2)], 'Color', 'r', 'LineWidth', 2, 'LineStyle', ':')
    line([.1 .1], [curYlim(1),curYlim(2)], 'Color', 'r', 'LineWidth', 2, 'LineStyle', ':')
    axis(handaxes2, 'off'); 
set(findall(gcf,'-property','FontSize'),'FontSize',18)
set(findall(handaxes2,'-property','FontSize'),'FontSize',8)
 
pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'D_RparamExample_Fig3';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
