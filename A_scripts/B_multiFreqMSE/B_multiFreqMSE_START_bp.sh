#!/bin/bash

# call the BOSC analysis by session and participant
cd /home/mpib/kosciessa/mseRhythmSimulation/A_scripts/B1_multiFreqMSE

VARIANTS="3"
FREQ="1 2 3 4 5 6 7"

for indVariant in $VARIANTS ; do
for indFreq in $FREQ ; do
	echo "#PBS -N MSE_Sim_${indVariant}_${indFreq}" 							> job
	echo "#PBS -l walltime=50:00:0" 								>> job
	echo "#PBS -l mem=16gb" 					    				>> job
	echo "#PBS -j oe" 												>> job
	echo "#PBS -o /home/mpib/kosciessa/mseRhythmSimulation/A_scripts/B1_multiFreqMSE/Y_logs" >> job
	echo "#PBS -m n" 												>> job
	echo "#PBS -d ." 												>> job
	echo "./B_multiFreqMSE_run.sh /opt/matlab/R2016b ${indVariant} ${indFreq}" >> job
	qsub job
	rm job # delete job file after execution
done
done
