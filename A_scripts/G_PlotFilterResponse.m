%% plot the magnitude filter response of the employed bandpass filter
% https://dsp.stackexchange.com/questions/16885/how-do-i-manually-plot-the-frequency-response-of-a-bandpass-butterworth-filter-i

h = figure('units','normalized','position',[.1 .1 .2 .25]);
set(0,'DefaultAxesColor',[.95 .95 .95])

N = 500;
nyq = N/2;

%% plot low-pass filter

[b,a] = butter(6,10/nyq, 'low');

% Create the vector of angular frequencies at one more point.
% After that remove the last element (Nyquist frequency)
w = linspace(0, pi, N+1);
w(end) = [];
ze = exp(-1j*w); % Pre-compute exponent
H = polyval(b, ze)./polyval(a, ze); % Evaluate transfer function and take the amplitude
Ha = abs(H);
Hdb  = 20*log10(Ha); % Convert to dB scale
wn = linspace(0, nyq, N); 

% Plot and set axis limits
%xlim = ([0 1]);
lowpass = plot(wn, Ha.^2, 'Color', [0 0 0], 'LineWidth', 2); % square magnitude function due to two-pass application
lpFilter = Ha.^2;
grid on
hold on;
cutoffFreq = line([10,10], [0,1], 'Color', [0 0 0], 'LineStyle', ':', 'LineWidth', 2);

xlim([0 20])
ylim([0 1])
%set(gca,'xscale','log')

%% additionally plot high-pass filter

[b,a] = butter(6,10/250, 'high');

N = 250; % Number of points to evaluate at
upp = pi; % Evaluate only up to fs/2
% Create the vector of angular frequencies at one more point.
% After that remove the last element (Nyquist frequency)
w = linspace(0, pi, N+1); 
w(end) = [];
ze = exp(-1j*w); % Pre-compute exponent
H = polyval(b, ze)./polyval(a, ze); % Evaluate transfer function and take the amplitude
Ha = abs(H);
Hdb  = 20*log10(Ha); % Convert to dB scale (JQK: this does not already account for squaring the magnitude function using filtfilt)
%wn   = w/pi;
wn = linspace(0, 250, N); 

% Plot and set axis limits
%xlim = ([0 1]);
highpass = plot(wn, Ha.^2, 'Color', [1 0 0], 'LineWidth', 2);
hpFilter = Ha.^2;

%% add bandpass response
scale = 25;

fcLowPass = (1./scale).*nyq + .05*((1./scale).*nyq);
fcHighPass = (1./(scale+1)).*nyq - .05*((1./(scale+1)).*nyq);

[b1,a1]=cheby1(4,1,fcLowPass/nyq, 'low');    % Lowpass
[b2,a2]=cheby1(4,1,fcHighPass/nyq,'high');   % Highpass

b = conv(b1,b2);
a = conv(a1,a2);
N = 250; % Number of points to evaluate at
% Create the vector of angular frequencies at one more point.
% After that remove the last element (nyquistuist frequency)
w = linspace(0, pi, N+1);
w(end) = [];
ze = exp(-1j*w); % Pre-compute exponent
H = polyval(b, ze)./polyval(a, ze); % Evaluate transfer function and take the amplitude
Ha = abs(H);
Hdb  = 20*log10(Ha); % Convert to dB scale
wn = linspace(0, 250, N); 

% Plot and set axis limits
%xlim = ([0 1]);
bandpass = plot(wn, Ha.^2, 'Color', [.5 .5 .5], 'LineWidth', 2); % square magnitude function due to two-pass application
bpFilter = Ha.^2;
grid on

passbandFreq = line([fcLowPass,fcLowPass], [0,1], 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 2);
passbandFreq = line([fcHighPass,fcHighPass], [0,1], 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 2);

%% finalize figure

leg = legend([lowpass, highpass,bandpass, cutoffFreq, passbandFreq], ...
    {'Butterworth low-pass'; 'Butterworth high-pass'; 'Chebyshev band-pass'; 'Cut-off frequency'; 'pass-band'},...
    'location', 'East', 'EdgeColor', [.95 .95 .95]); %legend('boxoff');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
set(leg,'FontSize',14)

xlabel('Frequency [Hz]'); ylabel('Magnitude Attenuation (linear)')
title('Filter responses at 10 Hz')

ylim([0 1])

pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'G_FilterResponse';
h.InvertHardcopy = 'off';
h.Color = 'white';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% Plot bandpass filter across entire filter range (Chebychev implementation)

set(0,'DefaultAxesColor',[.8 .8 .8])

cBrew = flipud(brewermap(42,'OrRd'));
fs = 500; nyq = fs/2;
h = figure('units','normalized','position',[.1 .1 .2 .25]);
cla; hold on;
for scale = 1:42
    fcLowPass = (1./scale).*nyq + .05*((1./scale).*nyq);
    fcHighPass = (1./(scale+1)).*nyq - .05*((1./(scale+1)).*nyq);
    FilterLimitsByScale(scale,:) = [fcLowPass, fcHighPass];
    if scale == 1
        fcHighPass = (1/(scale+1))*nyq;
        fcLowPass = nyq;
        [b2,a2]=butter(10,fcHighPass/nyq,'high');   % Highpass
        b = b2;
        a = a2;
    else
        if fcLowPass-fcHighPass > .05*nyq
            [b1,a1]=butter(10,fcLowPass/nyq, 'low');    % Lowpass
            [b2,a2]=butter(10,fcHighPass/nyq,'high');   % Highpass
        else
            [b1,a1]=cheby1(4,1,fcLowPass/nyq, 'low');    % Lowpass
            [b2,a2]=cheby1(4,1,fcHighPass/nyq,'high');   % Highpass
        end
        b = conv(b1,b2);
        a = conv(a1,a2);
    end
    N = 250; % Number of points to evaluate at
    % Create the vector of angular frequencies at one more point.
    % After that remove the last element (nyquistuist frequency)
    w = linspace(0, pi, N+1);
    w(end) = [];
    ze = exp(-1j*w); % Pre-compute exponent
    H = polyval(b, ze)./polyval(a, ze); % Evaluate transfer function and take the amplitude
    Ha = abs(H);
    Hdb  = 20*log10(Ha); % Convert to dB scale
    wn = linspace(0, nyq, N); 

    % Plot and set axis limits
    %xlim = ([0 1]);
    bandpass = plot(wn, Ha.^2, 'Color', cBrew(scale,:),'LineWidth', 2); % square magnitude function due to two-pass application
    bpFilter = Ha.^2;
    grid on
end

ylim([0 1])
set(gca,'xscale','log')
xlim([2 nyq])

xlabel('Frequency [Hz]'); ylabel('Magnitude Attenuation (linear)')
title({'Filter response of bandpass filter';['for different passbands (fs = ',num2str(fs),' Hz)']})

set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'G_FilterResponseBandpass';
h.InvertHardcopy = 'off';
h.Color = 'white';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% Supplement: filter properties of original Butterworth implementation in Kosciessa et al. (biorxiv preprint)

set(0,'DefaultAxesColor',[.8 .8 .8])

cBrew = flipud(brewermap(42,'OrRd'));
fs = 500; nyq = fs/2;
h = figure('units','normalized','position',[.1 .1 .2 .25]);
cla; hold on;
for scale = 1:42
    fcLowPass = (1./(scale)).*nyq;
    fcHighPass = (1./(scale+1)).*nyq;
    
    FilterLimitsByScale(scale,:) = [fcLowPass, fcHighPass];
    if scale == 1
        fcLowPass = nyq;
        [b2,a2]=butter(6,fcHighPass/nyq,'high');   % Highpass
        b = b2;
        a = a2;
    else
        [b1,a1]=butter(6,fcLowPass/nyq, 'low');          % Lowpass
        [b2,a2]=butter(6,fcHighPass/nyq,'high');   % Highpass
        b = conv(b1,b2);
        a = conv(a1,a2);
    end
    N = 250; % Number of points to evaluate at
    % Create the vector of angular frequencies at one more point.
    % After that remove the last element (nyquistuist frequency)
    w = linspace(0, pi, N+1);
    w(end) = [];
    ze = exp(-1j*w); % Pre-compute exponent
    H = polyval(b, ze)./polyval(a, ze); % Evaluate transfer function and take the amplitude
    Ha = abs(H);
    Hdb  = 20*log10(Ha); % Convert to dB scale
    wn = linspace(0, nyq, N); 

    % Plot and set axis limits
    %xlim = ([0 1]);
    bandpass = plot(wn, Ha.^2, 'Color', cBrew(scale,:),'LineWidth', 2); % square magnitude function due to two-pass application
    bpFilter = Ha.^2;
    grid on
end

ylim([0 1])
set(gca,'xscale','log')
xlim([2 nyq])

xlabel('Frequency [Hz]'); ylabel('Magnitude Attenuation (linear)')
title({'Filter response of bandpass filter';['for different passbands (fs = ',num2str(fs),' Hz)']})

set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'G_FilterResponseBandpass_Butterworth';
h.InvertHardcopy = 'off';
h.Color = 'white';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

