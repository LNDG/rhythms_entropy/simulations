%% Plot rhythm-mMSE relationship

% Plot simulation results for (a) different amplitudes of alpha rhythms and (b) different frequencies of fixed amplitude.

%% set relevant paths

    restoredefaultpath; clc;

    pn.rootDir      = '/Volumes/LNDG/Projects/mseRhythmSimulation/';
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];

%% compare low-pass, high-pass & band-pass

    for indFreq = 1:7
        % load low-pass
        load([pn.data, 'B1_multiFreqMSE_v3_',num2str(indFreq),'.mat'], 'mse');
        mse_low.mse{indFreq} = mse;
        % load high-pass
        load([pn.data, 'B1_multiFreqMSE_hp_v3_',num2str(indFreq),'.mat'], 'mse');
        mse_high.mse{indFreq} = mse;
        % load band-pass
        load([pn.data, 'B1_multiFreqMSE_BP_v5_',num2str(indFreq),'.mat'], 'mse');
        mse_band.mse{indFreq} = mse;
        % load pointavg
        load([pn.data, 'B1_multiFreqMSE_pointavg_noRrescale_v3_',num2str(indFreq),'.mat'], 'mse');
        mse_pointavg.mse{indFreq} = mse;
        mse_pointavg.mse{indFreq}.r = mse_pointavg.mse{indFreq}.r';
        % load pointavg with rescaling
        load([pn.data, 'B1_multiFreqMSE_pointavg_v3_',num2str(indFreq),'.mat'], 'mse');
        mse_pointavg_rescale.mse{indFreq} = mse;
    end

    freqLimitUp = 125./[1:31];
    freqLimitDown = 125./[2:32];
    
    frequencies = {'1', '2.5', '5', '10', '20', '40', '80'};
    frequencies_num = [1,2.5,5,10,20,40,80];
    
    mse_low.mse{1}.timescales % are calculated as (scale/fsample)*1000
    
    addpath([pn.rootDir, 'T_tools/brewermap/'])
        
%% simulate different amplitudes of alpha (FIGURE 2)

    indFrequency = 4;
    
    cBrew = flipud(brewermap(15,'OrRd'));
    cBrewR = flipud(brewermap(15,'OrRd'));
    set(0,'DefaultAxesColor',[.7 .7 .7])
    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    subplot(2,5,1); title({'Point average';'global bounds'}); hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(mse_pointavg.mse{indFrequency}.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(mse_pointavg.mse{indFrequency}.sampen(15,:),3)),'Color', cBrew(15,:), 'LineWidth', 2);
        plot(squeeze(nanmean(mse_pointavg.mse{indFrequency}.sampen(1,:),3)), 'k', 'LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,1.4], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        set(gca,'xtick',[]); ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(nanmean(mse_pointavg.mse{indFrequency}.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(nanmean(mse_pointavg.mse{indFrequency}.r(15,:),3),1)),'Color', cBrewR(15,:),'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_pointavg.mse{indFrequency}.r(1,:),3),1)),'k', 'LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,3], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 3]);
        set(gca, 'YTick', .5:.5:2.5);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(mse_pointavg_rescale.mse{indFrequency}.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(mse_pointavg_rescale.mse{indFrequency}.sampen(15,:),3)), 'Color', cBrew(15,:),'LineWidth', 2);
        plot(squeeze(nanmean(mse_pointavg_rescale.mse{indFrequency}.sampen(1,:),3)), 'k','LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,1.4], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])

    subplot(2,5,7);
    cla; hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse{indFrequency}.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse{indFrequency}.r(15,:),3),1)),'Color', cBrewR(15,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse{indFrequency}.r(1,:),3),1)), 'k','LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,3], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 3]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(mse_low.mse{indFrequency}.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(mse_low.mse{indFrequency}.sampen(15,:),3)), 'Color', cBrew(15,:),'LineWidth', 2);
        plot(squeeze(nanmean(mse_low.mse{indFrequency}.sampen(1,:),3)), 'k','LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,1.4], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(nanmean(mse_low.mse{indFrequency}.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(nanmean(mse_low.mse{indFrequency}.r(15,:),3),1)), 'Color', cBrewR(15,:),'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse{indFrequency}.r(1,:),3),1)), 'k','LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,3], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 3]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
        for indAmp = 2:15
            plot(squeeze(nanmean(mse_high.mse{indFrequency}.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(mse_high.mse{indFrequency}.sampen(15,:),3)), 'Color', cBrew(15,:),'LineWidth', 2);
        plot(squeeze(nanmean(mse_high.mse{indFrequency}.sampen(1,:),3)), 'k','LineWidth', 2);
        ll{indFrequency} = line([find(freqLimitDown<frequencies_num(indFrequency),1, 'first'),find(freqLimitDown<frequencies_num(indFrequency),1, 'first')], [0,1.4], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,9);
    cla; hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(nanmean(mse_high.mse{indFrequency}.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(nanmean(mse_high.mse{indFrequency}.r(15,:),3),1)), 'Color', cBrewR(15,:),'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_high.mse{indFrequency}.r(1,:),3),1)), 'k','LineWidth', 2);
        ll{indFrequency} = line([find(freqLimitDown<frequencies_num(indFrequency),1, 'first'),find(freqLimitDown<frequencies_num(indFrequency),1, 'first')], [0,3], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); ylim([0 3]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(mse_band.mse{indFrequency}.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 1);
        end
        plot(squeeze(nanmean(mse_band.mse{indFrequency}.sampen(15,:),3)),'Color', cBrew(15,:), 'LineWidth', 2);
        plot(squeeze(nanmean(mse_band.mse{indFrequency}.sampen(1,:),3)), 'k','LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,1.4], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        for indAmp = 2:15
            plot(squeeze(nanmean(nanmean(mse_band.mse{indFrequency}.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 1);
        end
        l2 = plot(squeeze(nanmean(nanmean(mse_band.mse{indFrequency}.r(2,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        l15 = plot(squeeze(nanmean(nanmean(mse_band.mse{indFrequency}.r(15,:),3),1)), 'Color', cBrewR(15,:), 'LineWidth', 2);
        l0 = plot(squeeze(nanmean(nanmean(mse_band.mse{indFrequency}.r(1,:),3),1)), 'w','LineWidth', 2);
        l1 = plot(squeeze(nanmean(nanmean(mse_band.mse{indFrequency}.r(1,:),3),1)), 'k','LineWidth', 2);
        ll{indFrequency} = line([125/frequencies_num(indFrequency),125/frequencies_num(indFrequency)], [0,1.25], 'Color', [1 1 1], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 3]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)

    % move subplots closer together

    AxesHandle=findobj(h,'Type','axes');
    axisPos = [];
    leftShift = -.05; upShift = +.12;
    for indAx = 1:10
        axInd = 10-indAx+1;
        axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
        if mod(indAx,2)==1
            % shift only left
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
        elseif mod(indAx,2)==0
            % shift left and up
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
            % increase leftwards shift for next element
            leftShift = leftShift-.03;
        end
    end
    
    pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
    figureName = 'B1_RhythmOnOffComparison_v4';
    h.InvertHardcopy = 'off';
    h.Color = 'white';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
            
%% plot influence of frequency on MSE and R by method (FIGURE 3)
    
    indAmp = 3;

    cBrew = brewermap(7,'OrRd');
    cBrewR = brewermap(7,'OrRd');
    set(0,'DefaultAxesColor',[.95 .95 .95])
    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    subplot(2,5,1); title({'Point average';'global bounds'}); 
    hold on;
        for indFreq = 3:6
            plot(squeeze(nanmean(mse_pointavg.mse{indFreq}.sampen(indAmp,:),3)), '-', 'Color', cBrew(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(nanmean(mse_pointavg.mse{1}.sampen(1,:),3)), 'k', 'LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 1.4]);
        set(gca, 'YTick', .5:.5:1);
        set(gca,'xtick',[]) %xlabel('Scale (Hz)'); 
        ylabel('Sample Entropy');
        set(gca, 'XScale', 'log')
    subplot(2,5,6);
    hold on;
        for indFreq = 3:6
            plot(squeeze(mse_pointavg.mse{indFreq}.r(indAmp,:)), '-', 'Color', cBrewR(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(mse_pointavg.mse{1}.r(1,:)),'k', 'LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([.5 .8]);
        set(gca, 'YTick', [.5, .8]);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        set(gca, 'XScale', 'log')
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); 
    hold on;
        for indFreq = 3:6
            plot(squeeze(nanmean(mse_pointavg_rescale.mse{indFreq}.sampen(indAmp,:),3)), '-', 'Color', cBrew(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(nanmean(mse_pointavg_rescale.mse{1}.sampen(1,:),3)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
        set(gca, 'XScale', 'log')
    subplot(2,5,7);
    hold on;
        for indFreq = 3:6
            plot(squeeze(mse_pointavg_rescale.mse{indFreq}.r(indAmp,:)), '-', 'Color', cBrewR(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(mse_pointavg_rescale.mse{1}.r(1,:)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([.3 .8]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        set(gca, 'XScale', 'log')
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        for indFreq = 3:6
            plot(squeeze(nanmean(mse_low.mse{indFreq}.sampen(indAmp,:),3)), '-', 'Color', cBrew(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(nanmean(mse_low.mse{1}.sampen(1,:),3)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
        set(gca, 'XScale', 'log')
    subplot(2,5,8);
    hold on;
        for indFreq = 3:6
            plot(squeeze(mse_low.mse{indFreq}.r(indAmp,:)), '-', 'Color', cBrewR(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(mse_low.mse{1}.r(1,:)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([.3 .75]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        set(gca, 'XScale', 'log')
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); 
    hold on;
        for indFreq = 3:6
            plot(squeeze(nanmean(mse_high.mse{indFreq}.sampen(indAmp,:),3)), '-', 'Color', cBrew(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(nanmean(mse_high.mse{1}.sampen(1,:),3)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([find(freqLimitDown<frequencies_num(indFreq),1, 'first'),find(freqLimitDown<frequencies_num(indFreq),1, 'first')], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
        set(gca, 'XScale', 'log')
    subplot(2,5,9);
    hold on;
        for indFreq = 3:6
            plot(squeeze(mse_high.mse{indFreq}.r(indAmp,:)), '-', 'Color', cBrewR(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(mse_high.mse{1}.r(1,:)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([find(freqLimitDown<frequencies_num(indFreq),1, 'first'),find(freqLimitDown<frequencies_num(indFreq),1, 'first')], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 .7]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        set(gca, 'XScale', 'log')
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); 
    hold on;
        for indFreq = 3:6
            plot(squeeze(nanmean(mse_band.mse{indFreq}.sampen(indAmp,:),3)), '-', 'Color', cBrew(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(nanmean(mse_band.mse{1}.sampen(1,:),3)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
        set(gca, 'XScale', 'log')
    subplot(2,5,10);% title('R: Bandpass'); 
    cla; hold on;
        for indFreq = 3:6
            plot(squeeze(mse_band.mse{indFreq}.r(indAmp,:)), '-', 'Color', cBrewR(indFreq,:), 'LineWidth', 2);
        end
        plot(squeeze(mse_band.mse{1}.r(1,:)), 'k','LineWidth', 2);
        for indFreq = 3:6
            ll{indFreq} = line([125/frequencies_num(indFreq),125/frequencies_num(indFreq)], [0,1.4]);
            set(ll{indFreq}, 'LineStyle', ':')
            set(ll{indFreq}, 'LineWidth', 2)
            set(ll{indFreq}, 'Color', cBrew(indFreq,:))
        end
        set(gca, 'XTick', round(125./[125,40,20,10,5]));set(gca, 'XTickLabel', round([125,40,20,10,5])); xlim([1 31]); ylim([0 .4]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        set(gca, 'XScale', 'log')
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    
    % move subplots closer together

    AxesHandle=findobj(h,'Type','axes');
    axisPos = [];
    leftShift = -.05; upShift = +.12;
    for indAx = 1:10
        axInd = 10-indAx+1;
        axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
        if mod(indAx,2)==1
            % shift only left
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
        elseif mod(indAx,2)==0
            % shift left and up
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
            % increase leftwards shift for next element
            leftShift = leftShift-.03;
        end
    end
    
    pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
    figureName = 'C_AllMethods_ScaleToFreqMatch_v4';
    h.InvertHardcopy = 'off';
    h.Color = 'white';
    
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% plot exemplary power spectrum

    load([pn.data, 'A0A_alphaData.mat'], 'wavelet', 'simTFR');

    h = figure('units','normalized','position',[.1 .1 .3 .4]);
    plot(wavelet.F, squeeze(nanmean(nanmean(nanmean(simTFR.TF_pow(1:4,:,:,:),4),2),1)), 'k', 'LineWidth', 4)
    xlabel('Frequency [Hz]'); ylabel('Power [a.u.]');
    set(gca, 'XTick', 1:3:35);
    %set(gca, 'XTickLabel', round(wavelet.F(1:3:35)))
    xlim([3 18])
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    pn.plotFolder = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/C_figures/';
    figureName = '0_exemplaryPowerSpectrum';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% Supplement: plot exemplary link between R and entropy (across scales, Vanilla)
    
    h = figure('units','normalized','position',[.1 .1 .3 .4]);
    x = squeeze(nanmean(mse_pointavg.mse{4}.r(:,1),2));
    y = squeeze(nanmean(mse_pointavg.mse{4}.sampen(:,:),2));
    scatter(x, y, 75, 'filled', 'MarkerFaceColor', [0 0 0]);
    xlim([.5 2.6])
    xlabel('Variance Criterion (R parameter; a.u.)'); ylabel('Entropy (a.u.)')
    title('Variance criterion contrains entropy estimates')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/C_figures/';
    figureName = '0_exemplaryLinkREntropy';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
