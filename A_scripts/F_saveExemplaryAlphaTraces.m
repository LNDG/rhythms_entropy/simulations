% Check the SNR of added rhythmicity

%% set relevant paths

    pn.rootDir      = '/Volumes/LNDG/Projects/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];
    
%% load simulated simData

    load([pn.data, 'A0A_frequencyData.mat'], 'simData');
    simDataOrig = simData;

    frequencies = [1, 2.5, 5, 10, 20, 40, 80];
    
%% plot alpha example
    
    indFreq = 4;
    simData.trial = squeeze(simDataOrig.trial(:,indFreq,:))';
    dataCat = cat(3, simData.trial{:});

    h = figure('units','normalized','position',[.1 .1 .6 .3]);
    set(0,'DefaultAxesColor',[1 1 1])
    subplot(3,3,[1,4,7]); imagesc(simData.time{1},[],squeeze(nanmean(dataCat(:,:,5),3))); colormap('gray'); title({'Simulated signals for an exemplary trial'}); 
        xlabel('Time (s)'); ylabel('Rhythm Level'); yticklabels(num2str(str2double(get(gca, 'yticklabels'))-1)); xlim([0 3]);
        cb = colorbar('location', 'EastOutside'); set(get(cb,'xlabel'), 'string', 'Amplitude [a.u.]')
    subplot(3,3,[2:3]); plot(simData.time{1},squeeze(nanmean(dataCat(1,:,5),3)), 'k', 'LineWidth', 2); xlim([0 3]);title('Pure 1/f signal'); axis off;% ax = gca; ax.Visible = 'off';
    subplot(3,3,[5:6]); plot(simData.time{1},squeeze(nanmean(dataCat(3,:,5),3)), 'Color', [1 0 0], 'LineWidth', 2); xlim([0 3]);title('1/f signal + Rhythm level 2'); axis off;
    subplot(3,3,[8:9]); plot(simData.time{1},squeeze(nanmean(dataCat(7,:,5),3)), 'Color', [0 .5 1], 'LineWidth', 2); xlim([0 3]);title('1/f signal + Rhythm level 6'); axis off;
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
    figureName = 'Z_exemplaryAlphaTraces';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% plot global SNR

% calculate global SNR: (RMS signal/RMSnoise)^2

RMS = squeeze(nanmean(rms(dataCat,2),3)); % RMS across time, averaged across 'trials'
RMS = (RMS./repmat(RMS(1),15,1)).^2;

h = figure('units','normalized','position',[.1 .1 .11 .3]); cla; hold on;
    plot(0:14, RMS, 'k', 'LineWidth', 5)
    scatter(2, RMS(3), 300,'MarkerFaceColor', [1 0 0], 'MarkerEdgeColor', [1 0 0]);
    scatter(6, RMS(7), 300,'MarkerFaceColor', [0 .5 1], 'MarkerEdgeColor', [0 .5 1]);
    grid minor; box off;
    xlabel('Amplitude level'); ylabel('Global SNR');
    set(findall(gcf,'-property','FontSize'),'FontSize',26)
    xlim([0 14])

pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'Z_exemplaryAlphaTraces_globalSNR';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
    
%% estimate SNR for each of the simulated rhythms at each level via FFT

% for indFreq = 1:7
%     
%     simData.trial = squeeze(simDataOrig.trial(:,indFreq,:))';
%     
%     cfg.output         = 'pow';
%     cfg.method         = 'mtmfft';
%     cfg.taper          = 'dpss';
%     cfg.tapsmofrq      = 2;
%     cfg.keeptrials     = 'yes';
%     cfg.trials         = 'all';
%     cfg.foi            = [1:.5:100];
%     freq               = ft_freqanalysis(cfg, simData);
%     
%     figure; hold on;
%     for indAmp = 1:7
%         plot(log10(freq.freq), squeeze(nanmean(log10(freq.powspctrm(:,indAmp,:)),1)))
%     end
%     
%     figure;
%     hold on;
%     for indAmp = 1:2:3
%         plot(freq.freq, squeeze(nanmean(freq.powspctrm(:,indAmp,:),1)))
%     end
%       
%     meanMeasure = squeeze(nanmedian(freq.powspctrm./repmat(freq.powspctrm(:,1,:),1,15,1),1));
%     
%     figure; imagesc(cfg.foi,[],meanMeasure); colorbar;

% end