%% add relevant paths

addpath(genpath('/Volumes/LNDG/Projects/mseRhythmSimulation/T_tools/SampEn'))
addpath(genpath('/Volumes/LNDG/Projects/mseRhythmSimulation/T_tools/legendmarkeradjust/'))

%% plot for white noise signal

% generate noise
mu=0;sigma=1;
noise= sigma *randn(1,500)+mu;

r = .5;

R1 = r.*std(noise);
R2 = r.*std(noise);

points = [1:3];
timeSamples = 50;

% get sample entropy alongside 2 and 3 point patterns

[Samp1, D_out1] = SampEn_withD_out(2, R1, noise(1:timeSamples));
[Samp2, D_out2] = SampEn_withD_out(2, R2, noise(1:timeSamples));

h = figure('units','normalized','position',[.1 .1 .3 .2]);
cla; hold on;
    time = 1/500:1/500:timeSamples/500;
    patches.ampVec = [noise(points(1))-(R2); noise(points(1))+(R2);...
        noise(points(2))-(R2); noise(points(2))+(R2);...
        noise(points(3))-(R2); noise(points(3))+(R2)];
    patches.timeVec = [time(1); time(timeSamples); time(2); time(timeSamples); time(3); time(timeSamples)];
    patches.colorVec =[.9 .9 .9; 1 1 1; .85 .85 .85; 1 1 1; .8 .8 .8]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
        p.FaceAlpha = .7;
    end
    % indicate edges usidn broken lines
    for indP = 1:2:size(patches.ampVec,1)-1
        ll1 = line([patches.timeVec(indP,1) patches.timeVec(indP+1,1)], [patches.ampVec(indP,1) patches.ampVec(indP,1)]);
        set(ll1, 'Color',patches.colorVec(indP,:), 'LineWidth', 2, 'LineStyle', ':')
        ll2 = line([patches.timeVec(indP,1) patches.timeVec(indP+1,1)], [patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)]);
        set(ll2, 'Color',patches.colorVec(indP,:), 'LineWidth', 2, 'LineStyle', ':')
    end
    tmpSignal = noise(1:1:timeSamples);
    plot(time, tmpSignal, 'k', 'LineWidth', 1, 'Marker', 'o','MarkerFaceColor',[1 1 1], 'MarkerEdgeColor', [0 0 0])
    scatter(time(points(1)), tmpSignal(1,points(1)), 150, 'filled','diamond','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0])
    leg1 = scatter(time(points(2)), tmpSignal(1,points(2)), 150, 'filled','diamond','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0]);
    title({['wide bounds, low sample entropy (', num2str(round(Samp2,2)),')']})
    xlabel('Time (s)'); ylabel('Amplitude (a.u.)');
    set(findall(gcf,'-property','FontSize'),'FontSize',17)
    % add remaining two-and three point patterns
    for indTwo = 1:size(D_out2{2,1},2)
        scatter(time(D_out2{2,1}(indTwo)+1), tmpSignal(D_out2{2,1}(indTwo)+1), 120, 'filled','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0])
        leg3 = scatter(time(D_out2{2,1}(indTwo)+2), tmpSignal(D_out2{2,1}(indTwo)+2), 120, 'filled','MarkerFaceColor',[0 0 0], 'MarkerEdgeColor', [0 0 0]);
    end
    for indTwo = 1:size(D_out2{2,1},2)
        if max(ismember(D_out2{3,1},D_out2{2,1}(indTwo)))==1
            leg4 = scatter(time(D_out2{2,1}(indTwo)+3), tmpSignal(D_out2{2,1}(indTwo)+3), 120, 'filled','pentagram','MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
        else
            leg5 = scatter(time(D_out2{2,1}(indTwo)+3), tmpSignal(D_out2{2,1}(indTwo)+3), 120, 'filled','pentagram','MarkerFaceColor',[1,0,0], 'MarkerEdgeColor', [0 0 0]);
        end
    end
    leg2 = scatter(time(points(3)), tmpSignal(points(3)), 150, 'filled','diamond', 'MarkerFaceColor',[.5,1,0], 'MarkerEdgeColor', [0 0 0]);
    legend('off')
%     set(legendHandle, 'FontSize', 12)
%     legendIcons = findobj(legendIcons, 'type', 'patch'); %// objects of legend of type line
%     set(legendIcons, 'Markersize', 14); %// set marker size as desired

%% perform a simulation:
% randomly vary SD of the signal

SDs = 1:.1:3;
SampEn = []; SampEn2 = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:500
        mu=0;sigma=1;
        noise= sigma *randn(1,500)+mu;
        r = .5;
        BoundSimilarity = SDs(indSD)*(r.*std(noise));
        [Samp1, D_out1] = SampEn_withD_out(2, BoundSimilarity, noise(1:timeSamples));
        SampEn(indSD,indTrial) = Samp1;
        [Samp2, D_out2] = SampEn_withD_out(2, (r.*std(noise)), noise(1:timeSamples));
        SampEn2(indSD,indTrial) = Samp2;
    end 
end

h = figure('units','normalized','position',[.1 .1 .15 .25]); cla; hold on; 
    l1 = plot(SDs, squeeze(nanmean(SampEn,2)), '-','Color',[0 0 0] ,'LineWidth', 4);
    plot(SDs, squeeze(nanmean(SampEn,2)), 'o','MarkerSize',10, 'MarkerFaceColor', [0 0 0],'MarkerEdgeColor', [0 0 0])

    l2 = plot(SDs, squeeze(nanmean(SampEn2,2)), '-','Color',[.5 .5 .5] ,'LineWidth', 4);
    plot(SDs, squeeze(nanmean(SampEn2,2)), 'o','MarkerSize',10, 'MarkerFaceColor', [.5 .5 .5],'MarkerEdgeColor', [.5 .5 .5])

    xlabel({'Scaling Factor'}); ylabel('Sample Entropy')
    title({'SampEn is sensitive to';'similarity bound scaling'})
    ylim([-.3 1.5])
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    leg = legend([l1, l2], {'Similarity bound = SF*(r*SD)'; 'Similarity bound = 1*(r*SD)'}, 'orientation', 'vertical', 'location', 'SouthWest', 'FontSize', 20); legend('boxoff')
    set(leg,'FontSize',15)

pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'Z_SampEnSensitivityToSimilarityBound';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% perform the inverse simulation: keep threshold constant, vary signal SD
% This is basically the mechanism behind fixed bound biases on coarse-scale entropy

SDs = 1:-.05:.2;
SampEn = []; SampEn2 = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:500
        mu=0;sigma=1;
        noise= sigma*randn(1,500)+mu;
        noiseReduced = SDs(indSD).*noise;
        r = .5;
        BoundSimilarity = (r.*std(noise));
        [Samp1, D_out1] = SampEn_withD_out(2, BoundSimilarity, noiseReduced(1:timeSamples));
        SampEn(indSD,indTrial) = Samp1;
        [Samp2, D_out2] = SampEn_withD_out(2, BoundSimilarity, noise(1:timeSamples));
        SampEn2(indSD,indTrial) = Samp2;
    end 
end

h = figure('units','normalized','position',[.1 .1 .15 .25]); cla; hold on; 
    l1 = plot(SDs, squeeze(nanmean(SampEn,2)), '-','Color',[0 0 0] ,'LineWidth', 4);
    plot(SDs, squeeze(nanmean(SampEn,2)), 'o','MarkerSize',10, 'MarkerFaceColor', [0 0 0],'MarkerEdgeColor', [0 0 0])
    l2 = plot(SDs, squeeze(nanmean(SampEn2,2)), '-','Color',[.5 .5 .5] ,'LineWidth', 4);
    plot(SDs, squeeze(nanmean(SampEn2,2)), 'o','MarkerSize',10, 'MarkerFaceColor', [.5 .5 .5],'MarkerEdgeColor', [.5 .5 .5])
    xlabel('Signal SD'); ylabel('Sample Entropy')
    title({'SampEn reduces for fixed bounds';'when signal SD decreases'});
    ylim([-.3 1.5]); xlim([.2 1])
    leg = legend([l1, l2], {'Bound = (r*SD);\newlineSD = 1'; 'Bound = (r*SD);\newlineSD = signal SD'}, 'orientation', 'vertical', 'location', 'SouthWest', 'FontSize', 20); legend('boxoff')
    set(gca, 'XDir','reverse')
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(leg,'FontSize',15)

pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'Z_SampEnSensitivityToBoundMismatch';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% randomly vary the r

Rs = .1:.1:2;

for indR = 1:numel(Rs)
    for indTrial = 1:5000
        mu=0;sigma=1;
        noise= sigma *randn(1,500)+mu;
        r = Rs(indR);
        [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
        SampEn_R(indR,indTrial) = Samp1;
    end 
end

figure; scatter(Rs, squeeze(nanmean(SampEn_R,2)), 'filled')
xlabel('R, SD = 1'); ylabel('SE, scale 1')

%%

addpath('/Volumes/LNDG/Programs_Tools_Scripts/Scripts_review before add to repo')

stimtime = 1;
samplingrate = 250;
lowcut = 2;
highcut = 125;
filtorder = 6;
NoiseVector_White = NoiseGenerator(stimtime,samplingrate,1,lowcut,highcut,filtorder);
NoiseVector_Pink = NoiseGenerator(stimtime,samplingrate,2,lowcut,highcut,filtorder);
NoiseVector_Blue = NoiseGenerator(stimtime,samplingrate,3,lowcut,highcut,filtorder);

figure; 
subplot(3,1,1); plot(NoiseVector_Blue); title('Blue noise')
subplot(3,1,2); plot(NoiseVector_White); title('White noise')
subplot(3,1,3); plot(NoiseVector_Pink); title('Pink noise')

% fourier transform to sanity-check

signals = {'NoiseVector_Blue'; 'NoiseVector_White'; 'NoiseVector_Pink'};

for indSignal = 1:numel(signals)
    X = eval(signals{indSignal});
    Y = fft(X);
    P2 = abs(Y/numel(X));
    P1 = P2(1:numel(X)/2+1);
    P1(2:end-1) = 2*P1(2:end-1);
    f = samplingrate*(0:(numel(X)/2))/numel(X);
    spectrum(indSignal,:) = P1;
end

figure; 
subplot(3,1,1); plot(f,spectrum(1,:)); title('Blue noise'); xlabel('Frequency (Hz)'); ylabel('Amplitude')
subplot(3,1,2); plot(f,spectrum(2,:)); title('White noise'); xlabel('Frequency (Hz)'); ylabel('Amplitude')
subplot(3,1,3); plot(f,spectrum(3,:)); title('Pink noise'); xlabel('Frequency (Hz)'); ylabel('Amplitude')



SDs = .1:1:100;

spectrum = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:500
        stimtime = 1;
        samplingrate = 250;
        lowcut = 2;
        highcut = 125;
        filtorder = 6;
        noise = NoiseGenerator(stimtime,samplingrate,1,lowcut,highcut,filtorder);
        noise = SDs(indSD).*noise;
        
        % FFT
        X = noise;
        Y = fft(X);
        P2 = abs(Y/numel(X));
        P1 = P2(1:numel(X)/2+1);
        P1(2:end-1) = 2*P1(2:end-1);
        f = samplingrate*(0:(numel(X)/2))/numel(X);
        spectrum(indSD,indTrial,:) = P1;

        % calculate sample entropy
        r = .5;
        [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
        SampEn(indSD,indTrial) = Samp1;
    end 
end

figure; scatter(SDs, squeeze(nanmean(SampEn,2)), 'filled')
xlabel('SD, r = .5'); ylabel('SE, scale 1')


figure; plot(squeeze(nanmean(spectrum,2))'); title('white noise spectra'); xlabel('Frequency'); ylabel('Amplitude')

SDs = .1:1:100;

SampEn_Pink = []; spectrum_pink = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:500
        stimtime = 1;
        samplingrate = 250;
        lowcut = 2;
        highcut = 125;
        filtorder = 6;
        noise = NoiseGenerator(stimtime,samplingrate,2,lowcut,highcut,filtorder);
        alpha_sim = 1.*sin([1/250:1/250:1]*2*pi*10); % THIS IS THE FORMULA FOR THE SINE
        %alpha_sim = SDs(indSD).*alpha_sim;
        noise = noise+alpha_sim;
        noise = SDs(indSD).*noise;
       
        % FFT
        X = noise;
        Y = fft(X);
        P2 = abs(Y/numel(X));
        P1 = P2(1:numel(X)/2+1);
        P1(2:end-1) = 2*P1(2:end-1);
        f = samplingrate*(0:(numel(X)/2))/numel(X);
        spectrum_pink(indSD,indTrial,:) = P1;
        
        % calculate sample entropy
        r = .5;
        [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
        SampEn_Pink(indSD,indTrial) = Samp1;
    end 
end

figure; scatter(SDs, squeeze(nanmean(SampEn_Pink,2)), 'filled')
xlabel('SD, r = .5'); ylabel('SE, scale 1'); title('Pink noise signal')

figure; plot(squeeze(nanmean(spectrum_pink,2))'); title('pink noise spectra'); xlabel('Frequency'); ylabel('Amplitude')

%% blue noise

SampEn_Blue = []; spectrum_blue = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:500
        stimtime = 1;
        samplingrate = 250;
        lowcut = 2;
        highcut = 125;
        filtorder = 6;
        noise = NoiseGenerator(stimtime,samplingrate,3,lowcut,highcut,filtorder);
        noise = SDs(indSD).*noise;
        
        % FFT
        X = noise;
        Y = fft(X);
        P2 = abs(Y/numel(X));
        P1 = P2(1:numel(X)/2+1);
        P1(2:end-1) = 2*P1(2:end-1);
        f = samplingrate*(0:(numel(X)/2))/numel(X);
        spectrum_blue(indSD,indTrial,:) = P1;
        
        % SE calculation
        r = .5;
        [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
        SampEn_Blue(indSD,indTrial) = Samp1;
    end 
end

figure; scatter(SDs, squeeze(nanmean(SampEn_Blue,2)), 'filled')
xlabel('SD, r = .5'); ylabel('SE, scale 1'); title('Blue noise signal')

figure; plot(squeeze(nanmean(spectrum_blue,2))'); title('blue noise spectra'); xlabel('Frequency'); ylabel('Amplitude')

%% pink noise + alpha

SDs = 0:.1:3;

SampEn_Pink_alpha = []; spectrum_pink_alpha = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:500
        stimtime = 1;
        samplingrate = 250;
        lowcut = 2;
        highcut = 125;
        filtorder = 6;
        noise = NoiseGenerator(stimtime,samplingrate,2,lowcut,highcut,filtorder);
        alpha_sim = 1.*sin([1/250:1/250:1]*2*pi*10); % THIS IS THE FORMULA FOR THE SINE
        alpha_sim = SDs(indSD).*alpha_sim;
        noise = noise+alpha_sim;
        %noise = SDs(indSD).*noise;
       
        % FFT
        X = noise;
        Y = fft(X);
        P2 = abs(Y/numel(X));
        P1 = P2(1:numel(X)/2+1);
        P1(2:end-1) = 2*P1(2:end-1);
        f = samplingrate*(0:(numel(X)/2))/numel(X);
        spectrum_pink_alpha(indSD,indTrial,:) = P1;
        
        % calculate sample entropy
        r = .5;
        [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
        SampEn_Pink_alpha(indSD,indTrial) = Samp1;
    end 
end

figure; plot(squeeze(nanmean(spectrum_pink_alpha,2))'); title('blue noise spectra'); xlabel('Frequency'); ylabel('Amplitude')


figure; hold on;
scatter(.1:1:100, squeeze(nanmean(SampEn_Pink,2)), 'filled', 'MarkerFaceColor', [1 0 0]); l1 = line([1,100],[nanmean(nanmean(SampEn_Pink,2),1), nanmean(nanmean(SampEn_Pink,2),1)], 'Color', [1 0 0]);
scatter(.1:1:100, squeeze(nanmean(SampEn,2)), 'filled', 'MarkerFaceColor', [0 0 0]); l2 = line([1,100],[nanmean(nanmean(SampEn,2),1), nanmean(nanmean(SampEn,2),1)], 'Color', [0 0 0]);
scatter(.1:1:100, squeeze(nanmean(SampEn_Blue,2)), 'filled', 'MarkerFaceColor', [0 1 1]); l3 = line([1,100],[nanmean(nanmean(SampEn_Blue,2),1), nanmean(nanmean(SampEn_Blue,2),1)], 'Color', [0 1 1]);
ylim([0 1.4])
ax1 = gca; % current axes
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'Color','none');
hold on; scatter(0:.1:3, squeeze(nanmean(SampEn_Pink_alpha,2)),'Parent',ax2, 'filled', 'MarkerFaceColor', [.5 .5 .5]);% l3 = line([1,100],[nanmean(nanmean(SampEn_Pink_alpha,2),1), nanmean(nanmean(SampEn_Pink_alpha,2),1)], 'Color', [0 1 1]);
ylim([0 1.4])

legend([l1,l2,l3],{'pink noise'; 'white noise'; 'blue noise'}, 'location', 'SouthWest'); legend('boxoff');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
xlabel('SD, r = .5'); ylabel('SE, scale 1'); title('Slope variations')


%% modulate phase but retain variance

% SampEn_Pink_phaseAligned = []; spectrum_pink_phase = [];
% for indSD = 1:numel(SDs)
%     for indTrial = 1:500
%         stimtime = 1;
%         samplingrate = 250;
%         lowcut = 2;
%         highcut = 125;
%         filtorder = 6;
%         noise = NoiseGenerator(stimtime,samplingrate,2,lowcut,highcut,filtorder);
%         noise = SDs(indSD).*noise;
%        
%         % FFT
%         X = noise;
%         Y = fft(X);
%         comp1 = real(Y);
%         comp2 = imag(Y);
%         comp2 = repmat(.5*pi,1,numel(comp2));
%         X = real(ifft(complex(comp1,comp2)));
%         Y = fft(X);
% 
%         P2 = abs(Y/numel(X));
%         P1 = P2(1:numel(X)/2+1);
%         P1(2:end-1) = 2*P1(2:end-1);
%         f = samplingrate*(0:(numel(X)/2))/numel(X);
%         spectrum_pink_phase(indSD,indTrial,:) = P1;
%         
%         % calculate sample entropy
%         r = .5;
%         [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
%         SampEn_Pink_phaseAligned(indSD,indTrial) = Samp1;
%     end 
% end
% 
% figure;
% subplot(1,2,1); scatter(SDs, squeeze(nanmean(SampEn_Pink_phaseAligned,2)), 'filled')
%     xlabel('SD, r = .5'); ylabel('SE, scale 1'); title('Pink noise signal')
% subplot(1,2,2); plot(squeeze(nanmean(spectrum_pink_phase,2))'); title('pink noise spectra'); xlabel('Frequency'); ylabel('Amplitude')
% 
% figure; hold on;
% scatter(SDs, squeeze(nanmean(SampEn_Pink,2)), 'filled', 'MarkerFaceColor', [1 0 0]); l1 = line([1,100],[nanmean(nanmean(SampEn_Pink,2),1), nanmean(nanmean(SampEn_Pink,2),1)], 'Color', [1 0 0]);
% scatter(SDs, squeeze(nanmean(SampEn_Pink_phaseAligned,2)), 'filled', 'MarkerFaceColor', [0 0 0]); l2 = line([1,100],[nanmean(nanmean(SampEn_Pink_phaseAligned,2),1), nanmean(nanmean(SampEn_Pink_phaseAligned,2),1)], 'Color', [0 0 0]);
% legend([l1,l2],{'pink noise'; 'pink noise phase aligned'}, 'location', 'west');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% xlabel('SD, r = .5'); ylabel('SE, scale 1'); title('Slope variations')

%% simulate relationship between variance and power for total signal

SDs = .1:1:100;

SampEn_Pink = []; spectrum_pink = [];
for indSD = 1:numel(SDs)
    for indTrial = 1:10
        stimtime = 1;
        samplingrate = 250;
        lowcut = 2;
        highcut = 125;
        filtorder = 6;
        noise = NoiseGenerator(stimtime,samplingrate,2,lowcut,highcut,filtorder);
%         alpha_sim = 1.*sin([1/250:1/250:1]*2*pi*10); % THIS IS THE FORMULA FOR THE SINE
%         %alpha_sim = SDs(indSD).*alpha_sim;
%         noise = noise+alpha_sim;
        noise = 3+(SDs(indSD).*noise);
       
        % FFT
        X = noise;
        Y = fft(X);
        P2 = abs(Y/numel(X));
        P1 = P2(1:numel(X)/2+1);
        P1(2:end-1) = 2*P1(2:end-1);
        f = samplingrate*(0:(numel(X)/2))/numel(X);
        spectrum_pink(indSD,indTrial,:) = P1;
        
        % total power 
        totalPower(indSD,indTrial) = sum(P1.^2);
        
        % calculate sample entropy
        r = .5;
        [Samp1, D_out1] = SampEn_withD_out(2, r.*std(noise), noise(1:timeSamples));
        SampEn_Pink(indSD,indTrial) = Samp1;
    end 
end

figure; 
scatter(nanmean(totalPower(:,1),2), SDs.^2, 'filled')