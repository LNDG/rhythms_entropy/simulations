%% Simulate different frequency signals for mMSE analysis

%% set relevant paths

    pn.rootDir = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    pn.FieldTrip = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data = [pn.rootDir, 'B_data/'];
    
     % previously simulated 1/f-filtered white noise spectrum
    load([pn.data, 'background.mat'],'bckgrnd_filt');
    
%%  parameters

frequencies = [1, 2.5, 5, 10, 20, 40, 80];

for indFreq = 1:numel(frequencies)

    SimParam.frequency = frequencies(indFreq);
    
    % create a time vector for the entire signal (NOTE: these have to remain fixed here due to the existing background simulations)
    timeDur = 8; % FIXED
    time = .004:.004:timeDur; % FIXED
    
    % wavelet parameters  
    wavelet.F          = 2.^[1:.125:5.25]; % sampled frequencies for wavelet estimation; currently uses log-scaling
    wavelet.wavenumber = 6;   % wavelet duration (i.e. important for time/frequency precision)
    wavelet.fsample    = 250; % sampling frequency; remains fixed
    
    SimParam.amplitude = 0:.5:7; % try for example .25; 1; 2; 5
    SimParam.cycles = floor(timeDur.*SimParam.frequency); 
    SimParam.iterations = 100;
    SimParam.scaleByBG = 0; % choose whether to APPROX. (!) simulation of SNR [alternative = 0; no control for background amplitude here]
    
    for a = 1:numel(SimParam.amplitude)
        c = 1; %a = 1; % only use one parameter here as the goal is visual demonstration
        tic
        for k = 1:SimParam.iterations
            disp(['Iteration ', num2str(k), '/', num2str(SimParam.iterations)]);
        %%  simulate sine with requested parameters
            if k == 1 % only simulate sine on the first iteration, as I retain the latency, amplitude and frequency across iterations
                sineCycles = SimParam.cycles(c);
                sineFreq = SimParam.frequency;
                sineTime(c) = round((sineCycles/sineFreq),3);
                % make simulatedSine symmetrical around the middle (not critical to understand here)
                    timeNew = round(sineTime(c)/0.004,0);
                    if mod(timeNew,2) ~= 0
                        timeNew = timeNew + 1;
                        sineTime(c) = timeNew.*0.004;
                    else sineTime(c) = timeNew.*0.004;
                    end
                % set up vector of time points during which the sine will be simulated
                timeAlpha = [.004:.004:sineTime(c)];
                alpha_sim = SimParam.amplitude(a).*sin(timeAlpha*2*pi*sineFreq); % THIS IS THE FORMULA FOR THE SINE
                % determine the location of the sinusoid within the overall time series
                SinePlace = (numel(time)/2)-(numel(timeAlpha)/2)+1:(numel(time)/2)+(numel(timeAlpha)/2);
                % create a vector that is the simulated sine at those locations and
                % zero everywhere else; this is the signal that will be added to
                % the pink noise background
                simulatedSine(1,:) = zeros(1,numel(time));
                simulatedSine(1,SinePlace) = alpha_sim;
            end

        %%  mix 1/f noise with simulated sine

            % notice that the background signal is of the same duration as the
            % vector containing the simulated sine wave

            % grab relevant background
            noise(k,:) = bckgrnd_filt(k,1:numel(simulatedSine));

            if SimParam.scaleByBG == 1
                % filter entire signal +- 2 Hz of simulated frequency (6th order butterworth)
                [tmp_b,tmp_a] = butter(6, [SimParam.frequency-2 SimParam.frequency+2]/(250/2), 'bandpass');
                tmp_bpsignal = filter(tmp_b,tmp_a,bckgrnd_filt(k,1:numel(simulatedSine)));
                SNRscale = 1/var(tmp_bpsignal);
                % scale BG to an approx. power of 1 in simulated frequency range; then add signal
                signal(k,:) = SNRscale*bckgrnd_filt(k,1:numel(simulatedSine)) + simulatedSine;
            else 
                % simply create signal by superimposing sine
                signal(k,:) = noise(k,:) + simulatedSine(1,:);
            end

            simData.trial{1,indFreq,k}(a,:) = signal(k,:);
            simData.label{1,a}              = num2str(SimParam.amplitude(a));
            simData.time{1,k}               = time;
            simData.fsample                 = 250;
            simdata.freq                    = frequencies;
        end
        disp(['Analysis ran:', num2str(toc), 's']);
    end
end
    
%% save output data

save([pn.data, 'A0A_frequencyData.mat'], 'simData', 'SimParam');
