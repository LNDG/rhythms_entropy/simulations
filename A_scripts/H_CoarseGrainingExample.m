
%% plot real timeseries

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/B_MSE_Segmented_Dim_Input/1117_EC_MSE_IN.mat')

pn.rootDir      = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;

cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = [40];
cfg.lpfiltord = 6;
cfg.lpfilttype = 'but';

data_lpf40 = ft_preprocessing(cfg, data);

cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = [20];
cfg.lpfiltord = 6;
cfg.lpfilttype = 'but';

data_lpf20 = ft_preprocessing(cfg, data);

cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = [8];
cfg.lpfiltord = 6;
cfg.lpfilttype = 'but';

data_lpf8 = ft_preprocessing(cfg, data);


indTrial = 40;
indChan = 59;
timePoints = 200:800;
%timePoints = 1:1:50;


h = figure('units','normalized','position',[.1 .1 .3 .4]);
subplot(4,1,1); cla; hold on;
    plot(data.time{1}(1:1:numel(timePoints))-data.time{1}(1), data.trial{indTrial}(indChan,timePoints), 'Color', [175/255, 36/255, 24/255], 'LineWidth', 3,'MarkerFaceColor',[1 1 1])
    set(gca,'XColor','none');
    set(gca,'YColor','none');
subplot(4,1,2); cla; hold on;
    plot(data.time{1}(1:1:numel(timePoints))-data.time{1}(1), data_lpf40.trial{indTrial}(indChan,timePoints), 'k', 'LineWidth', 3,'MarkerFaceColor',[1 1 1])
	set(gca,'XColor','none');
    set(gca,'YColor','none');
subplot(4,1,3); cla; hold on;
    plot(data.time{1}(1:1:numel(timePoints))-data.time{1}(1), data_lpf20.trial{indTrial}(indChan,timePoints), 'k', 'LineWidth', 3,'MarkerFaceColor',[1 1 1])
    set(gca,'XColor','none');
    set(gca,'YColor','none');
subplot(4,1,4); cla; hold on;
    plot(data.time{1}(1:1:numel(timePoints))-data.time{1}(1), data_lpf8.trial{indTrial}(indChan,timePoints), 'Color', [48/255, 111/255, 186/255], 'LineWidth', 3,'MarkerFaceColor',[1 1 1])
	set(gca,'XColor','none');
    set(gca,'YColor','none');
    
pn.plotFolder = '/Volumes/LNDG/Projects/mseRhythmSimulation/C_figures/';
figureName = 'Z_CoarseGrainingExample';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


