%% set relevant paths

clear all; clc; restoredefaultpath;

pn.rootDir = '/Users/kosciessa/Desktop/mntTardis/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
pn.FieldTrip = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
pn.mMSE = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
pn.NoiseTools = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
pn.data = [pn.rootDir, 'B_data/'];

addpath('/Volumes/LNDG/Projects/mseRhythmSimulation/T_tools')

%% define time vector for all following simulations

timeDur = .26; % FIXED
time = 0:.004:timeDur; % FIXED

%% simulate the following scenarios with all variants:

% - 1/f variations
% - 10 Hz on/off

% simulate 250 ms segments

% Different simulation parameters are encoded via different 'channels' such
% that the entropyfunction can run all simulated signals at once.

%% simulate rhythmic signals

alpha_sim = 1.*sin(time*2*pi*10); % THIS IS THE FORMULA FOR THE SINE

%% simulate 1/f

addpath('/Volumes/LNDG/Projects/mseRhythmSimulation/T_tools/');

spectralSlopes = [.5 1 1.2 1.5];

rng(202004)

PinkNoise = [];
for indSlope = 1:numel(spectralSlopes)
    for indIter = 1:100
        param.numOfElements = numel(time);
        param.variance = 1;
        param.exponent = spectralSlopes(indSlope);
        PinkNoise(indSlope,indIter,:) = f_alpha_gaussian(param.numOfElements, param.variance, param.exponent);
    end
end

%% add into fieldtrip structure

% combinatorials: 4 1/f spectra 
% - with/without alpha
% - with/without notch filtering

SimParam.iterations = 100;
simData = [];

indParam = 1;
for indSlope = 1:size(PinkNoise,1)
	for indAlpha = 1:2
        for indIter = 1:SimParam.iterations
            if indAlpha == 1
                simData.trial{1,indIter}(indParam,:) = squeeze(PinkNoise(indSlope,indIter,:));
            else
                simData.trial{1,indIter}(indParam,:) = squeeze(PinkNoise(indSlope,indIter,:))'+alpha_sim;
            end
            simData.label{1,indParam}       = ['slope_', num2str(indSlope), '_alpha_', num2str(indAlpha)];
            simData.time{1,indIter}         = time;
            simData.fsample                 = 250;
        end
        indParam = indParam + 1;
    end
end

%% save output data

save([pn.data, 'K_transientSlopeSimulation.mat'], 'simData');
