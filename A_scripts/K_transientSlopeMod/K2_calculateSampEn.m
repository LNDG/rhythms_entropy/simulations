function K2_calculateSampEn

  
%% set relevant paths

if ismac
    %pn.rootDir      = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    pn.rootDir      = '/Users/kosciessa/Desktop/mntTardis/mseRhythmSimulation/';
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];
else
    pn.rootDir      = '/home/mpib/kosciessa/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    %pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    %pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.data         = [pn.rootDir, 'B_data/'];
end
    
%% load simulated simData

    load([pn.data, 'K_transientSlopeSimulation.mat'], 'simData');
    
    simData.trialinfo = zeros(100,1); % mse function requires field
        
    %% run filskip mMSE on simulated data

    cfg                     = [];
    cfg.toi                 = .125;
    cfg.timwin              = .25;
    cfg.timescales          = 1;
    cfg.coarsegrainmethod   = 'filtskip';
    cfg.filtmethod          = 'lp';
    cfg.m                   = 2;
    cfg.r                   = 0.5;
    cfg.recompute_r         = 'perscale_toi_sp';

    mse = ft_entropyanalysis(cfg, simData);
    
    % plot 1 vs 1.2 vs 1 (no alpha)
    
    h = figure('units','normalized','position',[.1 .1 .2 .05]);
    subplot(1,2,1); cla; hold on;
    b1 = bar(1,[mse.sampen(3)]);
    b2 = bar(2,[mse.sampen(5)]);
    b3 = bar(3,[mse.sampen(3)]);
    set(b1,'FaceColor','k'); set(b1,'EdgeColor','k');
    set(b2,'FaceColor','r'); set(b2,'EdgeColor','r');
    set(b3,'FaceColor','k'); set(b3,'EdgeColor','k');
    ylim([.9 1.15])
    xticks([]); ylabel('SampEn')
    
    %h = figure('units','normalized','position',[.1 .1 .1 .05]);
    subplot(1,2,2); cla; hold on;
    b1 = bar(1,[mse.sampen(3)]);
    b2 = bar(2,[mse.sampen(4)]);
    b3 = bar(3,[mse.sampen(3)]);
    set(b1,'FaceColor','k'); set(b1,'EdgeColor','k');
    set(b2,'FaceColor','r'); set(b2,'EdgeColor','r');
    set(b3,'FaceColor','k'); set(b3,'EdgeColor','k');
     ylim([.9 1.15])
    xticks([]); ylabel('SampEn')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/F_periRhythmMSE/C_figures/';
    figureName = 'K2_validSlopeShift';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% now redo this with a notch filter
    
    cfg_hpf.bsfilter = 'yes';
    cfg_hpf.bsfreq = [8 15];
    cfg_hpf.bsfiltord = 6;
    [simData_notch] = ft_preprocessing(cfg_hpf, simData);
 
    cfg                     = [];
    cfg.toi                 = .125;
    cfg.timwin              = .25;
    cfg.timescales          = 1;
    cfg.coarsegrainmethod   = 'filtskip';
    cfg.filtmethod          = 'lp';
    cfg.m                   = 2;
    cfg.r                   = 0.5;
    cfg.recompute_r         = 'perscale_toi_sp';

    mse_notch = ft_entropyanalysis(cfg, simData_notch);
    
    h = figure('units','normalized','position',[.1 .1 .2 .05]);
    subplot(1,2,1); cla; hold on;
    b1 = bar(1,[mse_notch.sampen(3)]);
    b2 = bar(2,[mse_notch.sampen(5)]);
    b3 = bar(3,[mse_notch.sampen(3)]);
    set(b1,'FaceColor','k'); set(b1,'EdgeColor','k');
    set(b2,'FaceColor','r'); set(b2,'EdgeColor','r');
    set(b3,'FaceColor','k'); set(b3,'EdgeColor','k');
    ylim([.9 1.15])
    xticks([]); ylabel('SampEn')
    
    %h = figure('units','normalized','position',[.1 .1 .1 .05]);
    subplot(1,2,2); cla; hold on;
    b1 = bar(1,[mse_notch.sampen(3)]);
    b2 = bar(2,[mse_notch.sampen(4)]);
    b3 = bar(3,[mse_notch.sampen(3)]);
    set(b1,'FaceColor','k'); set(b1,'EdgeColor','k');
    set(b2,'FaceColor','r'); set(b2,'EdgeColor','r');
    set(b3,'FaceColor','k'); set(b3,'EdgeColor','k');
     ylim([.9 1.15])
    xticks([]); ylabel('SampEn')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    figureName = 'K2_invalidSlopeShift';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %save([pn.data, 'B1_multiFreqMSE_v3_',num2str(indFreq),'.mat'], 'mse');
 