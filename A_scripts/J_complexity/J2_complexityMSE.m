function B_multiFreqMSE

%% set relevant paths

if ismac
    pn.rootDir      = '/Volumes/LNDG/Projects/mseRhythmSimulation/';
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];
else
    pn.rootDir      = '/home/mpib/kosciessa/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    %pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    %pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.data         = [pn.rootDir, 'B_data/'];
end
    
%% load simulated simData

    load([pn.data, 'J_complexityData.mat'], 'simData');
    
    simData.trialinfo = zeros(100,1); % mse function requires field

    simDataOrig = simData;
    
    
        %% run filskip mMSE on simulated data

        cfg                     = [];
        cfg.toi                 = 4;
        cfg.timwin              = 8;
        cfg.timescales          = 1:42;
        cfg.coarsegrainmethod   = 'filtskip';
        cfg.filtmethod          = 'lp';
        cfg.m                   = 2;
        cfg.r                   = 0.5;
        cfg.recompute_r         = 'perscale_toi_sp';

        simData.trial = squeeze(simDataOrig.trial(:,indFreq,:))';
        mse = ft_entropyanalysis(cfg, simData);

        save([pn.data, 'B1_multiFreqMSE_v3_',num2str(indFreq),'.mat'], 'mse');
        
    