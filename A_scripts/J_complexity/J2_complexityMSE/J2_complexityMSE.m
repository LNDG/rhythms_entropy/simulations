function J2_complexityMSE(indVariant)

    % convert to numeric

    if ischar(indVariant) || isstring(indVariant)
        indVariant = str2num(indVariant);
    end

%% set relevant paths

if ismac
    %pn.rootDir      = '/Volumes/Kosciessa/TutorialsAndTests/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
    pn.rootDir      = '/Users/kosciessa/Desktop/mntTardis/mseRhythmSimulation/';
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];
else
    pn.rootDir      = '/home/mpib/kosciessa/mseRhythmSimulation/';
    pn.data         = [pn.rootDir, 'B_data/'];
end
    
%% load simulated simData

    load([pn.data, 'J_complexityData.mat'], 'simData');
    
    simData.trialinfo = zeros(100,1); % mse function requires field

    simDataOrig = simData;
    
    if indVariant == 1
    
        %% run filskip mMSE on simulated data

        cfg                     = [];
        cfg.toi                 = 4;
        cfg.timwin              = 8;
        cfg.timescales          = 1:42;
        cfg.coarsegrainmethod   = 'filtskip';
        cfg.filtmethod          = 'lp';
        cfg.m                   = 2;
        cfg.r                   = 0.5;
        cfg.recompute_r         = 'perscale_toi_sp';

        mse = ft_entropyanalysis(cfg, simData);

        save([pn.data, 'J2_complexityMSE_lp_v1.mat'], 'mse', 'cfg');
        
    elseif indVariant == 2

    %% run high pass mMSE on simulated data

        cfg                     = [];
        cfg.toi                 = 4;
        cfg.timwin              = 8;
        cfg.timescales          = 1:42;
        cfg.coarsegrainmethod   = 'filtskip';
        cfg.filtmethod          = 'hp';
        cfg.m                   = 2;
        cfg.r                   = 0.5;
        cfg.recompute_r         = 'perscale_toi_sp';

        mse = ft_entropyanalysis(cfg, simData);

        save([pn.data, 'J2_complexityMSE_hp_v1.mat'], 'mse', 'cfg');
    
    elseif indVariant == 3
        
        %% bandpass simulation

        cfg                     = [];
        cfg.toi                 = 4;
        cfg.timwin              = 8;
        cfg.timescales          = 1:42;
        cfg.coarsegrainmethod   = 'filtskip';
        cfg.filtmethod          = 'bp';
        cfg.m                   = 2;
        cfg.r                   = 0.5;
        cfg.recompute_r         = 'perscale_toi_sp';

        mse = ft_entropyanalysis(cfg, simData);

        save([pn.data, 'J2_complexityMSE_bp_v1.mat'], 'mse', 'cfg');

    elseif indVariant == 4
        
        %% pointavg simulation without r rescaling

        cfg                     = [];
        cfg.toi                 = 4;
        cfg.timwin              = 8;
        cfg.timescales          = 1:42;
        cfg.coarsegrainmethod   = 'pointavg';
        cfg.filtmethod          = 'no';
        cfg.m                   = 2;
        cfg.r                   = 0.5;
        cfg.recompute_r         = 'per_toi';

        mse = ft_entropyanalysis(cfg, simData);

        save([pn.data, 'J2_complexityMSE_vanilla_v1.mat'], 'mse', 'cfg');

    elseif indVariant == 5
        
     %% pointavg simulation with r rescaling

        cfg                     = [];
        cfg.toi                 = 4;
        cfg.timwin              = 8;
        cfg.timescales          = 1:42;
        cfg.coarsegrainmethod   = 'pointavg';
        cfg.filtmethod          = 'no';
        cfg.m                   = 2;
        cfg.r                   = 0.5;
        cfg.recompute_r         = 'perscale_toi_sp';

        mse = ft_entropyanalysis(cfg, simData);

        save([pn.data, 'J2_complexityMSE_pointavg_v1.mat'], 'mse', 'cfg');
        
    end
