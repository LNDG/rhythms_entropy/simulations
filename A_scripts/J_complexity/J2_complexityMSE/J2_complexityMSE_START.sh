#!/bin/bash

# call the BOSC analysis by session and participant
cd /home/mpib/kosciessa/mseRhythmSimulation/A_scripts/J_complexity/J2_complexityMSE/

VARIANTS="1 3 4"

for indVariant in $VARIANTS ; do
	echo "#PBS -N MSE_Sim_${indVariant}" 							> job
	echo "#PBS -l walltime=20:00:0" 								>> job
	echo "#PBS -l mem=16gb" 					    				>> job
	echo "#PBS -j oe" 												>> job
	echo "#PBS -o /home/mpib/kosciessa/mseRhythmSimulation/A_scripts/J_complexity/J2_complexityMSE/Y_logs" >> job
	echo "#PBS -m n" 												>> job
	echo "#PBS -d ." 												>> job
	echo "./J2_complexityMSE_run.sh /opt/matlab/R2016b ${indVariant}" >> job
	qsub job
	rm job # delete job file after execution
done

VARIANTS="2"

for indVariant in $VARIANTS ; do
	echo "#PBS -N MSE_Sim_${indVariant}" 							> job
	echo "#PBS -l walltime=160:00:0" 								>> job
	echo "#PBS -l mem=16gb" 					    				>> job
	echo "#PBS -j oe" 												>> job
	echo "#PBS -o /home/mpib/kosciessa/mseRhythmSimulation/A_scripts/J_complexity/J2_complexityMSE/Y_logs" >> job
	echo "#PBS -m n" 												>> job
	echo "#PBS -d ." 												>> job
	echo "./J2_complexityMSE_run.sh /opt/matlab/R2016b ${indVariant}" >> job
	qsub job
	rm job # delete job file after execution
done

VARIANTS="5"

for indVariant in $VARIANTS ; do
	echo "#PBS -N MSE_Sim_${indVariant}" 							> job
	echo "#PBS -l walltime=50:00:0" 								>> job
	echo "#PBS -l mem=16gb" 					    				>> job
	echo "#PBS -j oe" 												>> job
	echo "#PBS -o /home/mpib/kosciessa/mseRhythmSimulation/A_scripts/J_complexity/J2_complexityMSE/Y_logs" >> job
	echo "#PBS -m n" 												>> job
	echo "#PBS -d ." 												>> job
	echo "./J2_complexityMSE_run.sh /opt/matlab/R2016b ${indVariant}" >> job
	qsub job
	rm job # delete job file after execution
done


