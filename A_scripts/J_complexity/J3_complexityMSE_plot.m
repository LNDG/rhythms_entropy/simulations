%% Plot rhythm-mMSE relationship

% Plot simulation results for (a) different amplitudes of alpha rhythms and (b) different frequencies of fixed amplitude.

%% set relevant paths

    restoredefaultpath; clc;

    %pn.rootDir      = '/Volumes/LNDG/Projects/mseRhythmSimulation/';
    pn.rootDir      = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/mseRhythmSimulation/';
    pn.FieldTrip    = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
    pn.mMSE         = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
    pn.NoiseTools   = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
    pn.data         = [pn.rootDir, 'B_data/'];
    pn.plotFolder   = [pn.rootDir, 'C_figures/'];

%% compare low-pass, high-pass & band-pass

    % load low-pass
    load([pn.data, 'J2_complexityMSE_lp_v1.mat'], 'mse');
    mse_low.mse = mse;
    % load high-pass
    load([pn.data, 'J2_complexityMSE_hp_v1.mat'], 'mse');
    mse_high.mse = mse;
    % load band-pass
    load([pn.data, 'J2_complexityMSE_bp_v1.mat'], 'mse');
    mse_band.mse = mse;
    % load pointavg
    load([pn.data, 'J2_complexityMSE_vanilla_v1.mat'], 'mse');
    mse_pointavg.mse = mse;
    % load pointavg with rescaling
    load([pn.data, 'J2_complexityMSE_pointavg_v1.mat'], 'mse');
    mse_pointavg_rescale.mse = mse;

    freqLimitUp = 125./[1:42];
    freqLimitDown = 125./[1:42]+1;
    
    addpath([pn.rootDir, 'T_tools/brewermap/'])
    
    mse_low.mse.label
    
%% plot different 1/f slopes
    
    cBrew = flipud(brewermap(4,'Spectral'));
    cBrewR = flipud(brewermap(4,'Spectral'));
    set(0,'DefaultAxesColor',[.95 .95 .95])
    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    set(h, 'renderer', 'Painters')
    subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(mse_pointavg.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        set(gca,'xtick',[]); ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        for indAmp = 1:4
            l{indAmp} = plot(squeeze(mse_pointavg.mse.r(indAmp,:)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
        set(gca, 'YTick', .5:.5:2.5);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        %[.5 1 1.2 1.5];
        leg = legend([l{1}, l{2}, l{3}, l{4}],{'x = .5'; 'x = 1'; 'x = 1.2'; 'x = 1.5'}, 'Color', 'w', 'box', 'off', 'location', 'South');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(mse_pointavg_rescale.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])

    subplot(2,5,7);
    cla; hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(mse_low.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(nanmean(mse_low.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
        for indAmp = 1:4
            plot(squeeze(nanmean(mse_high.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,9);
    cla; hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(nanmean(mse_high.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(mse_band.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        for indAmp = 1:4
            plot(squeeze(nanmean(nanmean(mse_band.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)

    % move subplots closer together

    AxesHandle=findobj(h,'Type','axes');
    axisPos = [];
    leftShift = -.05; upShift = +.12;
    for indAx = 1:10
        axInd = 10-indAx+1;
        axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
        if mod(indAx,2)==1
            % shift only left
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
        elseif mod(indAx,2)==0
            % shift left and up
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
            % increase leftwards shift for next element
            leftShift = leftShift-.03;
        end
    end
    
    pn.plotFolder = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/mseRhythmSimulation/C_figures/J_complexity/';
    figureName = 'J3_MSE_by1f';
    h.InvertHardcopy = 'off';
    h.Color = 'white';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
            
%% plot rhythmic phase resets


%     cBrew = flipud(brewermap(20,'Spectral'));
%     cBrewR = flipud(brewermap(20,'Spectral'));
%     set(0,'DefaultAxesColor',[.95 .95 .95])
%     
%     h = figure('units','normalized','position',[.1 .1 .7 .5]);
%     subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(mse_pointavg.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
%         set(gca, 'YTick', .5:.5:1);
%         %xlabel('Scale (Hz)'); 
%         set(gca,'xtick',[]); ylabel('Sample Entropy');
%     subplot(2,5,6);
%     cla; hold on;
%         for indAmp = 13:20
%             l{indAmp} = plot(squeeze(nanmean(nanmean(mse_pointavg.mse.r(:,indAmp),3),2)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
%         set(gca, 'YTick', .5:.5:2.5);
%         xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
%         %[.5 1 1.2 1.5];
%         %leg = legend([l{1}, l{2}, l{3}, l{4}],{'x = .5'; 'x = 1'; 'x = 1.2'; 'x = 1.5'}, 'Color', 'w', 'box', 'off', 'location', 'South');
%     subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(mse_pointavg_rescale.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
%         %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
%         set(gca,'ytick',[]); set(gca,'xtick',[])
% 
%     subplot(2,5,7);
%     cla; hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
%         xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
%     subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(mse_low.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
%         %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
%         set(gca,'ytick',[]); set(gca,'xtick',[])
%     subplot(2,5,8);
%     cla; hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(nanmean(mse_low.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
%         xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
%     subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
%         for indAmp = 13:20
%             plot(squeeze(nanmean(mse_high.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); ylim([0 1.4]);
%         %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
%         set(gca,'ytick',[]); set(gca,'xtick',[])
%     subplot(2,5,9);
%     cla; hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(nanmean(mse_high.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); ylim([0 .5]);
%         xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
%     subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(mse_band.mse.sampen(indAmp,:),3)), 'Color', cBrew(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 1.4]);
%         %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
%         set(gca,'ytick',[]); set(gca,'xtick',[])
%     subplot(2,5,10);
%     cla; hold on;
%         for indAmp = 13:20
%             plot(squeeze(nanmean(nanmean(mse_band.mse.r(indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([0 .5]);
%         xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
%         %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
%     set(findall(gcf,'-property','FontSize'),'FontSize',23)
%     set(findall(leg,'-property','FontSize'),'FontSize',20)
% 
%     % move subplots closer together
% 
%     AxesHandle=findobj(h,'Type','axes');
%     axisPos = [];
%     leftShift = -.05; upShift = +.12;
%     for indAx = 1:10
%         axInd = 10-indAx+1;
%         axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
%         if mod(indAx,2)==1
%             % shift only left
%             set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
%         elseif mod(indAx,2)==0
%             % shift left and up
%             set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
%             % increase leftwards shift for next element
%             leftShift = leftShift-.03;
%         end
%     end
   
    
    cBrew = flipud(brewermap(4,'Spectral'));
    cBrewR = flipud(brewermap(4,'Spectral'));
    set(0,'DefaultAxesColor',[.95 .95 .95])

%     figure; hold on;
%     plot(squeeze(nanmean(nanmean(mse_band.mse.r(13,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.r(14,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
% 
%     
%     figure; hold on;
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(14,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(16,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(18,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(20,:),3),1)), 'Color', cBrewR(4,:), 'LineWidth', 2);
% %    figure; hold on;
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(13,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(17,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
%     plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(19,:),3),1)), 'Color', cBrewR(4,:), 'LineWidth', 2);

    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        set(gca,'xtick',[]); ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        for indAmp = 1:4
        	l{indAmp} = plot(squeeze(mse_pointavg.mse.r(((indAmp+6)*2),:))-squeeze(mse_pointavg.mse.r(((indAmp+6)*2-1),:)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 .5]);
        %set(gca, 'YTick', .5:.5:2.5);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        %[.5 1 1.2 1.5];
        %leg = legend([l{1}, l{2}, l{3}, l{4}],{'x = .5'; 'x = 1'; 'x = 1.2'; 'x = 1.5'}, 'Color', 'w', 'box', 'off', 'location', 'South');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,7);
    cla; hold on;
    	for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_low.mse.sampen(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.r(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_low.mse.r(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end  
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,9);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_band.mse.sampen(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.r(((indAmp+6)*2),:),3),1))-squeeze(nanmean(nanmean(mse_band.mse.r(((indAmp+6)*2-1),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        ll{1} = line([125/10,125/10], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)

    % move subplots closer together

    AxesHandle=findobj(h,'Type','axes');
    axisPos = [];
    leftShift = -.05; upShift = +.12;
    for indAx = 1:10
        axInd = 10-indAx+1;
        axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
        if mod(indAx,2)==1
            % shift only left
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
        elseif mod(indAx,2)==0
            % shift left and up
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
            % increase leftwards shift for next element
            leftShift = leftShift-.03;
        end
    end
    
    %% plot coupling shift
    
    cBrew = flipud(brewermap(4,'Spectral'));
    cBrewR = flipud(brewermap(4,'Spectral'));
    set(0,'DefaultAxesColor',[.95 .95 .95])
    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end    
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        %set(gca,'xtick',[]); 
        ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        for indAmp = 1:4
        	l{indAmp} = plot(squeeze(mse_pointavg.mse.r(((indAmp+2)*2-1),:))-squeeze(mse_pointavg.mse.r(((indAmp+2)*2),:)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
        set(gca, 'YTick', -.02:.02:.04);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        %[.5 1 1.2 1.5];
        leg = legend([l{1}, l{2}, l{3}, l{4}],{'x = .5'; 'x = 1'; 'x = 1.2'; 'x = 1.5'}, 'Color', 'w', 'box', 'off', 'location', 'South');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,7);
    cla; hold on;
    	for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_low.mse.sampen(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.r(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_low.mse.r(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end  
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,9);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_band.mse.sampen(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.r(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_band.mse.r(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)

    
    cBrew = flipud(brewermap(4,'Spectral'));
    cBrewR = flipud(brewermap(4,'Spectral'));
    set(0,'DefaultAxesColor',[.95 .95 .95])
    
    %% plot coupling shift (low)
    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end    
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        %set(gca,'xtick',[]); 
        ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        for indAmp = 1:4
        	l{indAmp} = plot(squeeze(mse_pointavg.mse.r(((indAmp+10)*2-1),:))-squeeze(mse_pointavg.mse.r(((indAmp+10)*2),:)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
        set(gca, 'YTick', -.02:.02:.04);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        %[.5 1 1.2 1.5];
        leg = legend([l{1}, l{2}, l{3}, l{4}],{'x = .5'; 'x = 1'; 'x = 1.2'; 'x = 1.5'}, 'Color', 'w', 'box', 'off', 'location', 'South');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,7);
    cla; hold on;
    	for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_low.mse.sampen(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.r(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_low.mse.r(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end  
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,9);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_band.mse.sampen(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.r(((indAmp+10)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_band.mse.r(((indAmp+10)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)
  
    %% rhythm phase reset
    
	figure; hold on;
    plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(1,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(13,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(14,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
	
    figure; hold on;
    plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(1,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(13,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(14,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
	
    
    figure; hold on;
    plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
	
    figure; hold on;
    plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(3,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(17,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(18,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
	
    
    figure; 
    subplot(1,2,1); hold on; title('Sample Entropy')
        plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(3,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(9,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(10,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
    subplot(1,2,2); hold on; title('R Param')
        plot(squeeze(nanmean(nanmean(mse_low.mse.r(3,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.r(9,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.r(10,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);

    figure; 
    subplot(1,2,1); hold on; title('Sample Entropy')
        plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
    subplot(1,2,2); hold on; title('R Param')
        plot(squeeze(nanmean(nanmean(mse_low.mse.r(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.r(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_low.mse.r(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);

    figure; 
    subplot(1,2,1); hold on; title('Sample Entropy')
        plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
    subplot(1,2,2); hold on; title('R Param')
        plot(squeeze(nanmean(nanmean(mse_band.mse.r(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_band.mse.r(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_band.mse.r(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);

        
    figure; 
    subplot(1,2,1); hold on; title('Sample Entropy')
        plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
    subplot(1,2,2); hold on; title('R Param')
        plot(squeeze(nanmean(nanmean(mse_high.mse.r(2,:),3),1)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_high.mse.r(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(nanmean(nanmean(mse_high.mse.r(16,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
        legend({'1/f', '1/f + sustained alpha', '1/f + phase-reset alpha'})

    
    plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(15,:),3),1)), 'Color', cBrewR(2,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(17,:),3),1)), 'Color', cBrewR(3,:), 'LineWidth', 2);
	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(19,:),3),1)), 'Color', cBrewR(4,:), 'LineWidth', 2);

    
    load([pn.data, 'J_complexityData.mat'], 'simData');

    figure; hold on; plot(simData.trial{1}(17,:)); plot(simData.trial{1}(18,:))
    figure; hold on; plot(simData.trial{1}(3,:)); plot(simData.trial{1}(17,:)); plot(simData.trial{1}(18,:))
    
    addpath('/Users/kosciessa/Downloads/ezfft')
    
    ezfft(.004:.004:8, simData.trial{1}(18,:))
    ezfft(.004:.004:8, simData.trial{1}(17,:))
    
    figure; hold on; plot(simData.trial{1}(17,:)); plot(simData.trial{1}(18,:)+10)

    
    figure; hold on; plot(simData.trial{1}(9,:)); plot(simData.trial{1}(10,:))
        
    
     h = figure('units','normalized','position',[.1 .1 .7 .5]);
     subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
        plot(squeeze(mse_pointavg.mse.sampen(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_pointavg.mse.sampen(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_pointavg.mse.sampen(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        set(gca,'xtick',[]); ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        l{1} = plot(squeeze(mse_pointavg.mse.r(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        l{2} = plot(squeeze(mse_pointavg.mse.r(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        l{3} = plot(squeeze(mse_pointavg.mse.r(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);    
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        set(gca, 'YTick', .5:.5:2.5);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        %[.5 1 1.2 1.5];
        leg = legend([l{1}, l{2}, l{3}],{'1/f', '1/f + sustained alpha', '1/f + phase-reset alpha'}, 'Color', 'w', 'box', 'off', 'location', 'South');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        plot(squeeze(mse_pointavg_rescale.mse.sampen(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_pointavg_rescale.mse.sampen(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_pointavg_rescale.mse.sampen(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,7);
    cla; hold on;
        plot(squeeze(mse_pointavg_rescale.mse.r(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_pointavg_rescale.mse.r(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_pointavg_rescale.mse.r(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        plot(squeeze(mse_low.mse.sampen(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_low.mse.sampen(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_low.mse.sampen(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        plot(squeeze(mse_low.mse.r(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_low.mse.r(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_low.mse.r(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);    
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
        plot(squeeze(mse_high.mse.sampen(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_high.mse.sampen(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_high.mse.sampen(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2); 
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,9);
    cla; hold on;
        plot(squeeze(mse_high.mse.r(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_high.mse.r(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_high.mse.r(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);     
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        plot(1:42,squeeze(mse_band.mse.sampen(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(1:42,squeeze(mse_band.mse.sampen(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(1:42,squeeze(mse_band.mse.sampen(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);     
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        plot(squeeze(mse_band.mse.r(2,:)), 'Color', cBrewR(1,:), 'LineWidth', 2);
        plot(squeeze(mse_band.mse.r(15,:)), 'Color', cBrewR(2,:), 'LineWidth', 2);
        plot(squeeze(mse_band.mse.r(16,:)), 'Color', cBrewR(3,:), 'LineWidth', 2);  
        ll{1} = line([125/5,125/5], get(gca,'ylim'), 'Color', [.5 .5 .5], 'LineStyle', ':', 'LineWidth', 4);
        set(gca, 'XTick', 1:12:42);set(gca, 'XTickLabel', round(freqLimitUp(1:12:42))); xlim([1 42]); %ylim([0 .5]);
        xlabel('Scale (Hz)'); set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)

    % move subplots closer together

    AxesHandle=findobj(h,'Type','axes');
    axisPos = [];
    leftShift = -.05; upShift = +.12;
    for indAx = 1:10
        axInd = 10-indAx+1;
        axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
        if mod(indAx,2)==1
            % shift only left
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
        elseif mod(indAx,2)==0
            % shift left and up
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
            % increase leftwards shift for next element
            leftShift = leftShift-.03;
        end
    end
    
    %%
    
     %% plot coupling shift
    
    cBrew = flipud(brewermap(4,'Spectral'));
    cBrewR = flipud(brewermap(4,'Spectral'));
    set(0,'DefaultAxesColor',[.95 .95 .95])
    
    h = figure('units','normalized','position',[.1 .1 .7 .5]);
    subplot(2,5,1); title({'Point average';'global bounds'}); cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg.mse.sampen(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end    
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %set(gca, 'YTick', .5:.5:1);
        %xlabel('Scale (Hz)'); 
        %set(gca,'xtick',[]); 
        ylabel('Sample Entropy');
    subplot(2,5,6);
    cla; hold on;
        for indAmp = 1:4
        	l{indAmp} = plot(squeeze(mse_pointavg.mse.r(28+indAmp,:)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);   %ylim([-.04 .02]);
        set(gca, 'YTick', -.02:.02:.04);
        xlabel('Scale (Hz)'); ylabel('Similarity Bound (r x SD)');
        %[.5 1 1.2 1.5];
        leg = legend([l{1}, l{2}, l{3}, l{4}],{'x = .5'; 'x = 1'; 'x = 1.2'; 'x = 1.5'}, 'Color', 'w', 'box', 'off', 'location', 'South');
    subplot(2,5,2); title({'Point average';'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.sampen(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,7);
    cla; hold on;
    	for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_pointavg_rescale.mse.r(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,3); title({'Lowpass'; 'scale-wise bounds'}); hold on;
        
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.sampen(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,8);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_low.mse.r(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end 
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);  %ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
%     subplot(2,5,4); title({'Highpass'; 'scale-wise bounds'}); hold on; % Note that x-axis labels are different now
%         
%         for indAmp = 1:4
%         	plot(squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.sampen(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end  
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
%         %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
%         %set(gca,'ytick',[]); set(gca,'xtick',[])
%     subplot(2,5,9);
%     cla; hold on;
%         for indAmp = 1:4
%         	plot(squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+2)*2-1),:),3),1))-squeeze(nanmean(nanmean(mse_high.mse.r(((indAmp+2)*2),:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
%         end      
%         set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitDown(1:12:31))); xlim([1 31]);  ylim([-.04 .02]);
%         xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
    subplot(2,5,5); title({'Bandpass'; 'scale-wise bounds'}); hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.sampen(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end      
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]); %ylim([0 1.4]);
        %xlabel('Scale (Hz)'); ylabel('Sample Entropy');
        %set(gca,'ytick',[]); set(gca,'xtick',[])
    subplot(2,5,10);
    cla; hold on;
        for indAmp = 1:4
        	plot(squeeze(nanmean(nanmean(mse_band.mse.r(28+indAmp,:),3),1)), 'Color', cBrewR(indAmp,:), 'LineWidth', 2);
        end   
        set(gca, 'XTick', 1:12:31);set(gca, 'XTickLabel', round(freqLimitUp(1:12:31))); xlim([1 31]);  %ylim([-.04 .02]);
        xlabel('Scale (Hz)'); %set(gca,'ytick',[]); %ylabel('Similarity Bound (r x SD)');
        %leg = legend([l1, l2, l15],{'1/f'; '1/f + alpha (1)'; '1/f + alpha (15)'}, 'Color', 'w', 'box', 'off', 'location', 'north');
    set(findall(gcf,'-property','FontSize'),'FontSize',23)
    set(findall(leg,'-property','FontSize'),'FontSize',20)
