%% set relevant paths

clear all; clc; restoredefaultpath;

pn.rootDir = '/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/mseRhythmSimulation/'; % NOTE: change path to folder with the file 'background.mat'
pn.FieldTrip = [pn.rootDir, 'T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
pn.mMSE = [pn.rootDir, 'T_tools/mMSE/']; addpath(pn.mMSE);
pn.NoiseTools = [pn.rootDir, 'T_tools/NoiseTools/']; addpath(genpath(pn.NoiseTools));
pn.data = [pn.rootDir, 'B_data/'];

addpath('/Volumes/LNDG/Projects/mseRhythmSimulation/T_tools')

%% define time vector for all following simulations

timeDur = 8; % FIXED
time = .004:.004:timeDur; % FIXED

%% simulate the following scenarios with all variants:

% - 1/f variations
% - pure rhythmic super-position (fixed amplitude)
% - phase-amplitude coupling (fixed amplitude, phase relationship)
% - phase resets

% Different simulation parameters are encoded via different 'channels' such
% that the entropyfunction can run all simulated signals at once.

%% simulate signals

rng(202002)
phaseFreq = 2:10; phaseFreq = phaseFreq(randi(numel(phaseFreq), 100,1));
ampFreq = 20:60; ampFreq = ampFreq(randi(numel(ampFreq), 100,1));

% randomly sample 100 trials with varying phase and amplitude frequencies
phaseCoupling{1} = []; phaseCoupling{2} = [];
for indIter = 1:100
    cfg.method = 'phalow_amphigh';
    cfg.randomseed = 202003;
    cfg.time = repmat({time},1,1);
    cfg.numtrl = 1;

    % s1: amplitude modulation (AM), frequency of this signal should be lower than s2
    % s2: second frequency, frequncy that becomes amplitude modulated
    % s3: DC shift of s1, should have frequency of 0

    cfg.s1.ampl  = 1;
    cfg.s1.freq  = phaseFreq(indIter);
    cfg.s2.freq  = ampFreq(indIter);
    cfg.s3.ampl  = 1;

    [phaseCouplingSignals] = ft_freqsimulation(cfg);
    phaseAmplitudeData = cat(3,phaseCouplingSignals.trial{:});
    phaseAmplitudeData = squeeze((phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:)).*phaseAmplitudeData(3,:,:))';
    phaseCoupling{1}(indIter,:) = zscore(phaseAmplitudeData');
    
    cfg.s1.ampl  = 0;
    cfg.s1.freq  = phaseFreq(indIter);
    cfg.s2.freq  = ampFreq(indIter);
    cfg.s3.ampl  = 1;

    [phaseCouplingSignals] = ft_freqsimulation(cfg);
    phaseAmplitudeData = cat(3,phaseCouplingSignals.trial{:});
    modulation = (phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:));
    phaseAmplitudeData = squeeze((phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:)).*phaseAmplitudeData(3,:,:))';
    phaseCoupling{2}(indIter,:) = zscore(phaseAmplitudeData');
    %phaseCoupling{2}(indIter,:) = phaseRandomize(phaseAmplitudeData');
end

% figure; indTrial = 5;
%     subplot(2,2,[1,3]); hold on;
%     y = phaseCoupling{1}(indTrial,:); Fs = 1/.004;L=length(y);N=ceil(log2(L));fy=fft(y,2^N)/(L/2);f=(Fs/2^N)*(0:2^(N-1)-1);
%     plot(f,abs(fy(1:2^(N-1))));
%     y = phaseCoupling{2}(indTrial,:); Fs = 1/.004;L=length(y);N=ceil(log2(L));fy=fft(y,2^N)/(L/2);f=(Fs/2^N)*(0:2^(N-1)-1);
%     plot(f,abs(fy(1:2^(N-1))));
%     subplot(2,2,[2]); hold on; plot(phaseCoupling{1}(indTrial,:))
%     subplot(2,2,[4]); hold on; plot(phaseCoupling{2}(indTrial,:))

%% repeat phase-amplitude coupling for lower frequency range

rng(202002)
phaseFreq = 1:.1:2; phaseFreq = phaseFreq(randi(numel(phaseFreq), 100,1));
ampFreq = 8:.5:12; ampFreq = ampFreq(randi(numel(ampFreq), 100,1));

% randomly sample 100 trials with varying phase and amplitude frequencies
phaseCoupling_low{1} = []; phaseCoupling_low{2} = [];
for indIter = 1:100
    cfg.method = 'phalow_amphigh';
    cfg.randomseed = 202003;
    cfg.time = repmat({time},1,1);
    cfg.numtrl = 1;

    % s1: amplitude modulation (AM), frequency of this signal should be lower than s2
    % s2: second frequency, frequncy that becomes amplitude modulated
    % s3: DC shift of s1, should have frequency of 0

    cfg.s1.ampl  = 1;
    cfg.s1.freq  = phaseFreq(indIter);
    cfg.s2.freq  = ampFreq(indIter);
    cfg.s3.ampl  = 1;

    [phaseCouplingSignals] = ft_freqsimulation(cfg);
    phaseAmplitudeData = cat(3,phaseCouplingSignals.trial{:});
    modulation = (phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:));
    phaseAmplitudeData = squeeze((phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:)).*phaseAmplitudeData(3,:,:))';
    phaseCoupling_low{1}(indIter,:) = zscore(phaseAmplitudeData');
    
    cfg.s1.ampl  = 0;
    cfg.s1.freq  = phaseFreq(indIter);
    cfg.s2.freq  = ampFreq(indIter);
    cfg.s3.ampl  = 1;

    [phaseCouplingSignals] = ft_freqsimulation(cfg);
    phaseAmplitudeData = cat(3,phaseCouplingSignals.trial{:});
    modulation = (phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:));
    phaseAmplitudeData = squeeze((phaseAmplitudeData(2,:,:)+phaseAmplitudeData(4,:,:)).*phaseAmplitudeData(3,:,:))';
    phaseCoupling_low{2}(indIter,:) = zscore(phaseAmplitudeData');
end

% figure; hold on; plot(phaseCoupling_low{1}(indIter,:)); plot(phaseCoupling_low{2}(indIter,:)+4)
% 
% figure; hold on; ezfft(time, zscore(phaseCoupling_low{1}(indIter,:)))
% ezfft(time, zscore(phaseCoupling_low{2}(indIter,:)))

%% simulate rhythmic phase resets (actually simulates 5 Hz)

rng(202003)

f = 10;
t = time;
randomTimes = randperm(numel(t));

phaseResets = [];

% simulate perfect theta wave
for i=1:100
    randomTimes = randperm(numel(t));
    PhaseResetTime = t(randomTimes(1:30));
    p = pi*f*t; % linear phase increase
    s = cos(p);
    d_orig(i,:) = s; % remember the signal on each repetition
end
phaseResets{1} = d_orig;

% simulate theta wave with phase resets
for i=1:100
    randomTimes = randperm(numel(t));
    PhaseResetTime = t(randomTimes(1:30));
    p = pi*f*t; % linear phase increase
    for indRandom = 1: numel(PhaseResetTime)
        p(t<PhaseResetTime(indRandom)) = p(t<PhaseResetTime(indRandom)) + 2*pi*rand(1); % random phase prior to TMS pulse
        p = p + cumsum(2*pi*(rand(size(p))-0.5)*0.01); % random walk
        p = p - p(t==PhaseResetTime(indRandom));
    end
    s = cos(p);
    d_orig(i,:) = s; % remember the signal on each repetition
end
phaseResets{2} = d_orig;

% figure;
% plot(t, mean(phaseResets,1))
% 
% figure; imagesc(phaseResets)

%% simulate 1/f

addpath('/Volumes/LNDG/Projects/mseRhythmSimulation/T_tools/');

spectralSlopes = [.5 1 1.2 1.5];

rng(202004)

PinkNoise = [];
for indSlope = 1:numel(spectralSlopes)
    for indIter = 1:100
        param.numOfElements = numel(time);
        param.variance = 1;
        param.exponent = spectralSlopes(indSlope);
        PinkNoise(indSlope,indIter,:) = f_alpha_gaussian(param.numOfElements, param.variance, param.exponent);
    end
end

% figure; hold on;
% for indSlope = 1:numel(spectralSlopes)
%     plot(squeeze(PinkNoise(indSlope,50,:))+2*indSlope.^2)
% end

%% simulate LRTCs

% amplitude time series as power law (multiplicative)

% A = zscore(squeeze(PinkNoise(4,2,:))'.*cos(time*2*pi*10));
% figure; plot(A)

%% add into fieldtrip structure

% combinatorials: 4 1/f spectra

% combinatorials: 4 1/f spectra
% phase coupling: yes/no [no needs to keep the same power spectrum, but shuffle phases]

% combinatorials: 4 1/f spectra
% alpha phase resets: yes/no [no needs to keep the same power spectrum, but shuffle phases]

SimParam.iterations = 100;
simData = [];

indParam = 1;
for indSlope = 1:size(PinkNoise,1)
    for indIter = 1:SimParam.iterations
        simData.trial{1,indIter}(indParam,:) = zscore(squeeze(PinkNoise(indSlope,indIter,:)),[],1);
        simData.label{1,indParam}       = ['slope_', num2str(indSlope)];
        simData.time{1,indIter}         = time;
        simData.fsample                 = 250;
    end
    indParam = indParam + 1;
end

%% add phase-amplitude coupling contrast

for indSlope = 1:size(PinkNoise,1)
    for indCoupling = 1:2
        for indIter = 1:SimParam.iterations
            simData.trial{1,indIter}(indParam,:) = zscore(squeeze(PinkNoise(indSlope,indIter,:)),[],1) + zscore(phaseCoupling{indCoupling}(indIter,:),[],2)';
            simData.label{1,indParam}       = ['slope_', num2str(indSlope), '_coupling_', num2str(indCoupling)];
        end
        indParam = indParam + 1;
    end
end

%% add alpha reset contrast
% 1 - no phase reset (continuous oscillation)
% 2 - phase reset (higher irregularity)

for indSlope = 1:size(PinkNoise,1)
    for indReset = 1:2
        for indIter = 1:SimParam.iterations
            simData.trial{1,indIter}(indParam,:) = zscore(squeeze(PinkNoise(indSlope,indIter,:)),[],1) + zscore(phaseResets{indReset}(indIter,:),[],2)';
            simData.label{1,indParam} = ['slope_', num2str(indSlope), '_reset_', num2str(indReset)];
        end
        indParam = indParam + 1;
    end
end
    
% figure; hold on; plot(simData.trial{1,indIter}(3,:)); plot(simData.trial{1,indIter}(18,:))
% figure; hold on; plot(simData.trial{1,indIter}(18,:)); plot(simData.trial{1,indIter}(19,:))
% figure; hold on; plot(simData.trial{1,indIter}(7,:)); plot(simData.trial{1,indIter}(8,:))
% figure; hold on; plot(simData.trial{1,indIter}(23,:)); plot(simData.trial{1,indIter}(24,:)+8)

% figure; hold on; ezfft(time, simData.trial{1,indIter}(7,:))
% ezfft(time,simData.trial{1,indIter}(8,:))
% 
% figure; hold on; ezfft(time, simData.trial{1,indIter}(23,:))
% ezfft(time,simData.trial{1,indIter}(24,:))
% 
%% add phase-amplitude coupling contrast (low)

for indSlope = 1:size(PinkNoise,1)
    for indCoupling = 1:2
        for indIter = 1:SimParam.iterations
            simData.trial{1,indIter}(indParam,:) = zscore(squeeze(PinkNoise(indSlope,indIter,:)),[],1) + zscore(phaseCoupling_low{indCoupling}(indIter,:),[],2)';
            simData.label{1,indParam} = ['slope_', num2str(indSlope), '_coupling2_', num2str(indCoupling)];
        end
        indParam = indParam + 1;
    end
end

%% add LRTC (filter LRTC signal with low-pass freq of 8 Hz)

for indSlope = 1:size(PinkNoise,1)
    for indIter = 1:SimParam.iterations
        x = squeeze(PinkNoise(indSlope,indIter,:))';
        [b,a] = butter(6,4/(250/2)); % Butterworth filter of order 6
        y = filter(b,a,x);
        simData.trial{1,indIter}(indParam,:) = zscore(y.*cos(time*2*pi*10));
        simData.label{1,indParam} = ['slope_', num2str(indSlope), '_DFA'];
    end
    indParam = indParam + 1;
end

% figure; plot(simData.trial{1,2}(32,:))
% figure; plot(simData.trial{1,3}(29,:))
% 
% addpath('/Users/kosciessa/Downloads/ezfft')
% figure; hold on; ezfft(time, simData.trial{1,indIter}(29,:))
% ezfft(time,simData.trial{1,indIter}(32,:))

%% save output data

save([pn.data, 'J_complexityData.mat'], 'simData');
