Scripts for the simulations of reflection of narrowband rhythms in modified Multi-scale Sample Entropy [MSE]
**Description of scripts**
-
A:  
- simulate rhythms of different frequencies and amplitudes

B:   
- calculate mMSE (on tardis cluster)

C:
- plot relation of rhythms to sample entropy estimates
- Figure 4
- Figure 5

D:
- plot empirical example of similarity thresholds for 1/f & high-frequency signals
- plot empirical example of slow fluctuation characterization in fine scales
- Figure 1B, Figure 3A

F:
- plot examples of simulated alpha signals
- Supplementary Figure 3

G:
- plot spectral attenuation of the filters applied at each scale
- Supplementary Figure 4

H: 
- plot exemplary filtered signals at multiple time scales

I: 
- simulate noise of different color and assess influence of similarity threshold scaling on entropy
- Supplementary Figure 2

J:
- simulate different 1/f variants
- try other simulations (unfinished; not reported)
- Supplementary Figure 7

K:
- simulate transient slope modulation (250 ms 1/f signal; 250 ms 1/f + alpha)
- calculate SampEn for different variants
- Figure 11A
